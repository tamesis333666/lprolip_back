<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibroNivelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libro_nivel', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('institucion_libro');
            $table->integer('nivel');
            $table->timestamps();
        });
        Schema::table('libro_nivel', function($table)
        {
            $table->foreign('institucion_libro')->references('id')->on('institucion_libro');
            $table->foreign('nivel')->references('idnivel')->on('nivel');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('libro_nivel');
    }
}
