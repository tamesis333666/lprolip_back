<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanlectorNivelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planlector_nivel', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('institucion_planlector');
            $table->integer('nivel');
            $table->timestamps();
        });
        Schema::table('planlector_nivel', function($table)
        {
            // $table->foreign('institucion_planlector')->references('id')->on('institucion_planlector');
            $table->foreign('nivel')->references('idnivel')->on('nivel');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planlector_nivel');
    }
}
