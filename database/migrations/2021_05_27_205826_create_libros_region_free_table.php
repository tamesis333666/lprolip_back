<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibrosRegionFreeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libros_region_free', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('libro');
            $table->integer('region');
            $table->boolean('estado');
            $table->timestamps();
        });
        Schema::table('libros_region_free', function($table)
        {
            $table->foreign('libro')->references('idlibro')->on('libro');
            $table->foreign('region')->references('idregion')->on('region');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('libros_region_free');
    }
}
