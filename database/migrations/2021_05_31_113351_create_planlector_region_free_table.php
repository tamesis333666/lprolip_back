<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanlectorRegionFreeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planlector_region_free', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('planlector');
            $table->integer('region');
            $table->boolean('estado');
            $table->timestamps();
        });
        Schema::table('planlector_region_free', function($table)
        {
            $table->foreign('planlector')->references('idplanlector')->on('planlector');
            $table->foreign('region')->references('idregion')->on('region');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planlector_region_free');
    }
}
