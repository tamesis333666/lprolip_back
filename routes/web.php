<?php
Route::get('/', function () {
    return view('auth.login');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/blog', 'HomeController@index')->name('blog');


 // Route::post('login','Auth\LoginController@login')->name('login');
// Route::get('/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
// //Rutas Usuario
// Route::get('/usuario','UsuarioController@index');
// Route::get('/prolipa','UsuarioController@prolipa');
// Route::get('/docente','UsuarioController@docente');
// Route::get('/estudiantes','UsuarioController@estudiantes');
// Route::get('usuarioActivar','UsuarioController@activar');
// Route::get('usuarioDesactivar','UsuarioController@desactivar');
// Route::get('usuariodato','UsuarioController@datosUsuario');
// Route::get('vendedor','UsuarioController@vendedor');
// Route::get('getvendedor','UsuarioController@getvendedor');
// Route::post('/usuarios/registrar', 'UsuarioController@store');
// Route::post('/usuarios/editar', 'UsuarioController@update');
// Route::post('/usuario/eliminar', 'UsuarioController@eliminarUsuario');
// Route::get('/usuario/papelera', 'UsuarioController@papeleraUsuario');
// Route::post('/usuario/restaurar', 'UsuarioController@restaurarUsuario');
// Route::post('guardarUsuarioConectado', 'UsuarioController@guardarUsuarioConectado');
// Route::post('eliminarUsuario/Conectado', 'UsuarioController@eliminarUsuarioConectado');

// Route::get('usuarioSelect', 'UsuarioController@select');
// Route::get('nivelSelect', 'NivelController@select');
// Route::get('docentes', 'UsuarioController@docentes');
// Route::get('historial', 'UsuarioController@historial');
// Route::get('historialI', 'UsuarioController@historialI');
// Route::get('datosUsuario', 'UsuarioController@datosUsuario');
// Route::post('/guardarPassword', 'UsuarioController@guardarPassword');

// //Rutas Institucion
// Route::get('institucion','InstitucionController@index');
// Route::get('Hinstitucion','InstitucionController@Hinstitucion');
// Route::get('institucionSelect', 'InstitucionController@select');
// Route::get('instituciondato', 'InstitucionController@datos');
// Route::post('institucion/registrar', 'InstitucionController@store');
// Route::post('institucion/save', 'InstitucionController@create');
// Route::get('institucion/vendedor', 'InstitucionController@intituciones_vendero');

// //Rutas Rol
// Route::get('rolSelect', 'RolController@select');
// //Rutas Ciudad
// Route::get('ciudadSelect', 'CiudadController@select');
// //Rutas Region
// Route::get('regionSelect', 'RegionController@select');
// //Rutas Area
// Route::get('areaSelect', 'AreaController@select');
// Route::get('areaS', 'AreaController@index');

// //Rutas Asignatura
// Route::get('asignaturaSelect', 'AsignaturaController@select');
// Route::resource('asignaturas', 'AsignaturaController');
// Route::post('asignaturas/eliminar', 'AsignaturaController@destroy');
// //Rutas Asignatura Docente
// Route::get('asignaturadocente','AsignaturaDocenteController@index');
// Route::post('/asignatura/rasignaturadocente', 'AsignaturaDocenteController@store');
// Route::post('/asignatura/eliminar', 'AsignaturaDocenteController@destroy');

// //Rutas Libro
// Route::get('libro','LibroController@index');
// Route::get('librosEstudiante','LibroController@librosEstudiante');

// Route::get('libro/registraringreso','LibroController@registraringreso');
// //Rutas Plan Lector
// Route::get('planlector','PlanLectorController@index');
// //Rutas Material de Apoyo
// Route::get('material','MaterialApoyoController@index');
// //Rutas Cuaderno
// Route::get('cuaderno','CuadernoController@index');
// //Rutas Guia
// Route::get('guia','GuiaController@index');
// //Rutas Planificacion
// Route::get('planificacion','PlanificacionController@index');
// //Rutas Planificacion
// Route::get('video','VideoController@index');
// //Rutas AudioLibro
// Route::get('libroA','LibroController@audio');
// Route::get('audios','AudioController@index');
// //Rutas Periodo
// Route::post('periodo/registrar', 'PeriodoController@store');
// Route::get('periodoSelect', 'PeriodoController@select');
// Route::get('periodoInstitucion', 'PeriodoController@institucion');
// Route::post('periodo/activar', 'PeriodoController@activar');
// Route::post('periodo/desactivar', 'PeriodoController@desactivar');
// //socket
// Route::get('idInstitucion', 'InstitucionController@idInstitucion');
// //Rutas PeriodoInstitucion
// Route::post('periodoinstitucion/registrar', 'PeriodoInstitucionController@create');
// Route::post('periodoinstitucion/eliminar', 'PeriodoInstitucionController@destroy');


// //login google
// Route::get('login/{provider}', 'SocialController@redirectToProvider');
// Route::get('login/{provider}/callback', 'SocialController@handleProviderCallback');
// //TablasRegistros
// Route::get('libros','LibroController@libro');
// Route::get('asignaturas','AsignaturaController@asignatura');
// Route::get('cuadernos','CuadernoController@cuaderno');
// Route::get('guias','GuiaController@guia');
// Route::get('planlectors','PlanLectorController@planlector');
// Route::get('planificaciones','PlanificacionController@planificacion');
// Route::get('materiales','MaterialApoyoController@material');
// Route::get('videos','VideoController@video');
// Route::get('audios','AudioController@audio');
// // Estado
// Route::get('estadoSelect', 'EstadoController@select');
// //Directirio
// Route::get('/Alibro', 'ArchivosController@libro');
// Route::get('/Aexe', 'ArchivosController@exe');

// Route::get('/Acuadernodigital', 'ArchivosController@cuadernodigital');
// Route::get('/Acuadernoexe', 'ArchivosController@cuadernoexe');

// Route::get('/Aguiadigital', 'ArchivosController@guiadigital');
// Route::get('/Aguiaexe', 'ArchivosController@guiaexe');

// Route::get('/Aplanlectordigital', 'ArchivosController@planlectordigital');
// Route::get('/Aplanlectorexe', 'ArchivosController@planlectorexe');

// Route::get('/Apdfguiadidactica', 'ArchivosController@pdfguiadidactica');
// Route::get('/Apdfsinguia', 'ArchivosController@pdfsinguia');
// Route::get('/Apdfconguia', 'ArchivosController@pdfconguia');

// Route::get('/Aplanificacion', 'ArchivosController@planificacion');
// Route::get('/Amaterialexe', 'ArchivosController@material');

// Route::get('/Abases','ArchivosController@bases');

// //Registros

// Route::post('libror/registrar', 'LibroController@store');
// Route::post('libror/update', 'LibroController@update');
// Route::post('libror/eliminar', 'LibroController@destroy');

// Route::post('cuadernor/registrar', 'CuadernoController@store');
// Route::post('cuadernor/update', 'CuadernoController@update');
// Route::post('cuadernor/eliminar', 'CuadernoController@destroy');

// Route::post('guiar/registrar', 'GuiaController@store');
// Route::post('guiar/update', 'GuiaController@update');
// Route::post('guiar/eliminar', 'GuiaController@destroy');

// Route::post('planlectorr/registrar', 'PlanLectorController@store');
// Route::post('planlectorr/update', 'PlanLectorController@update');
// Route::post('planlectorr/eliminar', 'PlanLectorController@destroy');

// Route::post('planificacionr/registrar', 'PlanificacionController@store');
// Route::post('planificacionr/update', 'PlanificacionController@update');
// Route::post('planificacionr/eliminar', 'PlanificacionController@destroy');

// Route::post('materialr/registrar', 'MaterialApoyoController@store');
// Route::post('materialr/update', 'MaterialApoyoController@update');
// Route::post('materialr/eliminar', 'MaterialApoyoController@destroy');

// Route::post('videor/registrar', 'VideoController@store');
// Route::post('videor/update', 'VideoController@update');
// Route::post('videor/eliminar', 'VideoController@destroy');

// Route::post('audior/registrar', 'AudioController@store');
// Route::post('audior/update', 'AudioController@update');
// Route::post('audior/eliminar', 'AudioController@destroy');

// Route::post('areas/registrar', 'AreaController@store');
// Route::post('niveles/registrar', 'NivelController@store');

// // Aplanicacion MAC
// Route::get('loginMac','UsuarioController@loginMac');

// //closeSession
// Route::get('closeSession','UsuarioController@closeSession');
// //Socket

// //Guardar Curso
// Route::post('cursoGuaradar', 'CursoController@create');
// Route::post('cursoGuaradarEditado', 'CursoController@update');
// Route::post('eliminarCurso', 'CursoController@delete');
// Route::get('curso', 'CursoController@index');

// //Informacion
// Route::get('accesoProlipa','InformacionController@accesoProlipa');
// Route::get('accesoUsuarios','InformacionController@accesoUsuarios');
// Route::get('institucionRegistradas','InformacionController@institucionRegistradas');
// Route::get('usuariosRegistrados','InformacionController@usuariosRegistrados');
// Route::get('numInstituciones','InformacionController@numInstituciones');

// //asignaturas clases autor
// Route::get('getAsignaturas','CursoLibroController@getAsignaturas');
// Route::get('getArea','CursoLibroController@getArea');
// Route::get('eliminarcursolibro','CursoLibroController@eliminar_libro_curso');

// //Reporte 
// Route::get('getReporte','UsuarioController@getReporte');
// Route::get('getCompleto','UsuarioController@getCompleto');
// Route::get('getReporteVisitas','UsuarioController@getReporteVisitas');

// //Historial
// Route::post('/postHistorialLibro', 'LibroController@Historial');
// Route::post('/postHistorialCuaderno', 'CuadernoController@Historial');
// Route::post('/postHistorialPlanlector', 'PlanLectorController@Historial');
// Route::post('/postHistorialMaterial', 'MaterialApoyoController@Historial');

// //Estudiante
// Route::get('estudianteCurso', 'EstudianteController@estudianteCurso');
// Route::get('cursoSugerencias', 'EstudianteController@cursoSugerencias');
// Route::get('tareasEstudiante/{id}', 'EstudianteController@tareasEstudiante');
// Route::post('addClase', 'EstudianteController@addClase');
// Route::post('addContenido', 'CursoController@addContenido');
// Route::post('addTareaContenido', 'CursoController@addTareaContenido');
// Route::post('guardarTarea', 'CursoController@guardarTarea');
// Route::post('postCalificacion', 'CursoController@postCalificacion');
// Route::get('getContenido','CursoController@getContenido');
// Route::get('getContenidoTodo','CursoController@getContenidoTodo');
// Route::get('getEstudiantes','CursoController@getEstudiantes');
// Route::get('getTareas','CursoController@getTareas');
// Route::get('getTareasDocentes','CursoController@getTareasDocentes');
// Route::get('eliminarContenido','CursoController@eliminarContenido');
// Route::post('eliminarAlumno','CursoController@eliminarAlumno');
// Route::get('eliminarTarea','CursoController@eliminarTarea');
// Route::get('librosCurso','CursoController@librosCurso');
// Route::get('librosCursoEliminar','CursoController@librosCursoEliminar');
// Route::post('postLibroCurso','CursoController@postLibroCurso');
// Route::resource('contenido','ContenidoController');
// //////////////////////////////////
// Route::get('buscar','ReporteUsuarioController@buscar');
// Route::get('recuperar','ReporteUsuarioController@recuperar');
// Route::resource('calificacion','CalificacionController');
// Route::resource('juegos','JuegosController');


