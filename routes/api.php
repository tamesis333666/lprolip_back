<?php

use Illuminate\Http\Request;

Route::get('posts', 		'PostController@index');
Route::get('post/{slug}', 	'PostController@show');
Route::group(['middleware' => 'auth.jwt'], function () {
    Route::post('/logout', 'AuthController@logout');
});

Route::group(['middleware' => 'cors'],function(){
    Route::post('addContenidopost', 'CursoController@addContenidoD');
    Route::post('/login', 'AuthController@login');
    Route::post('/register', 'AuthController@register');
    Route::apiResource('menu','MenuController');
    Route::apiResource('usr','UsuarioController');
    Route::apiResource('institucion','InstitucionController');
    Route::apiResource('admin','AdminController');
    Route::apiResource('vendedor','VendedorController');
    Route::apiResource('docente','DocenteController');
    Route::apiResource('estudiante','EstudianteController');
    Route::apiResource('contenido','ContenidoController');
    Route::get('teletareasunidades/{id}','ContenidoController@teletareasunidades');
    Route::get('teletareasunidades_tema/{id}','ContenidoController@teletareasunidades_tema');
    Route::get('teletarea_asignatura/{id}','ContenidoController@teletarea_asignatura');
    Route::apiResource('asignatura','AsignaturaController');
    Route::apiResource('curso','CursoController');
    Route::get('cursos_evaluaciones/{id}','CursoController@cursos_evaluaciones');
    Route::get('curso_asig_docente/{id}','CursoController@curso_asig_docente');
    Route::get('verif_asignatura_por_curso/{id}', 'CursoController@verif_asignatura_por_curso');
    Route::post('cargar_asignatura_curso', 'CursoController@cargar_asignatura_curso');
    Route::post('guardar_asignatura_usuario', 'AsignaturaDocenteController@guardar_asignatura_usuario');
    Route::get('asignaturas_crea_docente/{id}','AsignaturaDocenteController@asignaturas_crea_docente');
    Route::get('deshabilitarasignatura/{id}','AsignaturaDocenteController@deshabilitarasignatura');
    Route::apiResource('ciudad','CiudadController');
    Route::apiResource('region','RegionController');
    Route::apiResource('rol','RolController');
    Route::apiResource('periodo','PeriodoController');
    Route::apiResource('juegos','JuegosController');
    Route::get('juegos_tema/{id}','JuegosController@juegos_tema');
    Route::get('juegos_unidad/{id}','JuegosController@juegos_unidad');
    Route::get('juegos_asignatura/{id}', 'JuegosController@juegos_asignatura');
    Route::apiResource('libros','LibroController');
    Route::get('menu_unidades_libros/{id}','LibroController@menu_unidades_libros');    
    Route::get('unidades_asignatura/{id}','LibroController@unidades_asignatura'); 
    Route::get('desgloselibrousuario/{id}','LibroController@desgloselibrousuario');
    Route::get('periodoInstitucion', 'PeriodoController@institucion');

    Route::apiResource('cuadernos','CuadernoController');
    Route::post('cuadernos_usuario_libro', 'CuadernoController@cuadernos_usuario_libro');
    Route::apiResource('codigoslibros','CodigosLibrosController');
    Route::get('codigos_libros_estudiante/{id}','CodigosLibrosController@codigos_libros_estudiante');
    Route::post('addContenido', 'CursoController@addContenido');
    Route::get('getContenido','CursoController@getContenido');
    Route::get('getContenidoTodo','CursoController@getContenidoTodo');
    Route::get('eliminarContenido','CursoController@eliminarContenido');
    Route::get('librosEstudiante','LibroController@librosEstudiante');
    Route::post('postLibroCurso','CursoController@postLibroCurso');
    Route::get('librosCurso','CursoController@librosCurso');
    Route::get('librosCursoEliminar','CursoController@librosCursoEliminar');
    Route::get('getTareasDocentes','CursoController@getTareasDocentes');
    Route::post('postCalificacion', 'CursoController@postCalificacion');
    Route::post('quitarTareaEntregada', 'CursoController@quitarTareaEntregada');
    Route::get('getEstudiantes','CursoController@getEstudiantes');
    Route::get('estudianteCurso', 'EstudianteController@estudianteCurso');
    Route::post('estudiantesEvalCurso', 'EstudianteController@estudiantesEvalCurso');
    Route::get('tareaEstudiantePendiente','EstudianteController@tareaEstudiantePendiente');
    Route::get('tareaEstudianteRealizada','EstudianteController@tareaEstudianteRealizada');
    Route::post('addTareaContenido', 'CursoController@addTareaContenido');
    Route::get('tareas', 'DocenteController@tareas');
    Route::get('contenidos', 'DocenteController@contenidos'); 
    
    Route::get('calificacion', 'CursoController@Calificacion');
    Route::post('addClase', 'EstudianteController@addClase');
    Route::post('verificarCursoEstudiante', 'EstudianteController@verificarCursoEstudiante');
    Route::get('selectInstitucion','InstitucionController@selectInstitucion');
    Route::get('estudiantejuegos','JuegosController@juegosEstudainte');
    Route::post('guardarTarea','CursoController@guardarTarea');
    Route::post('restaurar', 'UsuarioController@restaurar');
    Route::post('cambio_password', 'UsuarioController@passwordC');
    Route::post('perfil', 'UsuarioController@perfil');
    Route::post('quitarTareaEntregada', 'CursoController@quitarTareaEntregada');
    Route::post('curso_libro_docente', 'CursoController@curso_libro_docente');
    Route::get('areaSelect', 'AreaController@select');
    
    
    
    // ===================== API ==========================
    Route::apiResource('cursolibro','CursoLibroController');
    Route::get('libro','LibroController@aplicativo');
    Route::get('selectlibro','LibroController@libro');
    Route::get('selectplanlector','LibroController@planlector');
    Route::post('libroFree','LibroController@libroFree');
    Route::post('planlectorFree','LibroController@planlectorFree');
    Route::get('listaFree','LibroController@listaFree');
    Route::get('listaFreePlanlector','LibroController@listaFreePlanlector');
    Route::post('setNivelFree','LibroController@setNivelFree');
    Route::get('eliminarLibroFree','LibroController@eliminarLibroFree');
    Route::get('eliminarPlanlectorFree','LibroController@eliminarPlanlectorFree');
    Route::get('libroEstudiante','LibroController@aplicativoEstudiante');
    Route::post('quitarlibroestudiante','LibroController@quitarlibroestudiante');
    Route::get('cuaderno','CuadernoController@aplicativo');
    Route::get('guia','GuiaController@aplicativo');
    Route::get('planlector','PlanLectorController@aplicativo');
    Route::get('material','MaterialApoyoController@aplicativo');
    Route::get('materialapoyo_unidad/{id}','MaterialApoyoController@materialapoyo_unidad');
    Route::get('materialapoyolibro_tema/{id}','MaterialApoyoController@materialapoyolibro_tema');
    Route::get('planificacion','PlanificacionController@aplicativo');
    Route::get('planificacion_asignatura/{id}','PlanificacionController@planificacion_asignatura');
    Route::get('video','VideoController@aplicativo');
    Route::get('videos_libro_unidad/{id}','VideoController@videos_libro_unidad');
    Route::get('videos_libro_tema/{id}','VideoController@videos_libro_tema');
    Route::get('usuario','UsuarioController@aplicativo');
    Route::get('aplicativobase','UsuarioController@aplicativobase');
    Route::get('usuarios','UsuarioController@index');
    Route::apiResource('notaEstudiante','NotaEstudianteController');
    Route::get('buscaUsuario','UsuarioController@buscaUsuario');
    Route::get('ciudades','CiudadController@ciudades');
    Route::get('verInstitucionCiudad/{id}','InstitucionController@verInstitucionCiudad');
    Route::get('verificarInstitucion/{id}','InstitucionController@verificarInstitucion');
    Route::post('asignarInstitucion','InstitucionController@asignarInstitucion');
    Route::apiResource('seminario', 'SeminarioController');
    Route::apiResource('inscripcion', 'InscripcionController');
    Route::apiResource('nivel', 'NivelController');
    Route::get('buscarSeminario', 'SeminarioController@buscarSeminario');
    Route::get('eliminarSeminario', 'SeminarioController@eliminarSeminario');
    Route::get('asignaturas','AsignaturaController@asignatura');
    Route::get('eliminarTarea','CursoController@eliminarTarea');
    Route::get('eliminarCurso','CursoController@eliminarCurso');
    Route::post('eliminarAlumno','CursoController@eliminarAlumno');
    Route::post('setContenido','ContenidoController@setContenido');

    //apis evaluaciones    
    Route::apiResource('evaluacion', 'EvaluacionController');
    Route::apiResource('pregunta', 'PreguntaController');
    Route::get('preguntasDocente/{id}', 'PreguntaController@preguntasDocente');
    Route::apiResource('tema', 'TemaController');
    Route::post('temasignunidad','TemaController@temasignunidad');
    Route::get('temAsignaruta/{id}','TemaController@temAsignaruta');
    Route::post('eliminar_tema','TemaController@eliminar_tema');
    Route::apiResource('pregEvaluacion', 'PregEvaluacionController');
    Route::post('pregEvaluacionGrupo', 'PregEvaluacionController@pregEvaluacionGrupo');
    Route::post('preguntasxbanco', 'PregEvaluacionController@preguntasxbanco');
    Route::post('preguntasxbancoDocente', 'PregEvaluacionController@preguntasxbancoDocente');
    Route::post('preguntasxbancoProlipa', 'PregEvaluacionController@preguntasxbancoProlipa');
    Route::post('pregEvaluacionEstudiante', 'PregEvaluacionController@pregEvaluacionEstudiante');    
    Route::apiResource('respEvaluacion', 'CalificacionEvalController');
    Route::post('verifRespEvaluacion', 'CalificacionEvalController@verifRespEvaluacion');
    Route::apiResource('evaluacionResponder', 'EvaluacionController');
    Route::post('cargarOpcion', 'PreguntaController@cargarOpcion');
    Route::get('quitarOpcion/{id}','PreguntaController@quitarOpcion');
    Route::post('editarOpcion','PreguntaController@editarOpcion');
    Route::get('verOpciones/{id}','PreguntaController@verOpciones');
    Route::get('evaluacionEstudiante/{id}', 'CalificacionEvalController@evaluacionEstudiante');
    Route::get('quitarPregEvaluacion/{id}','PregEvaluacionController@quitarPregEvaluacion');
    Route::post('evaluacionesDocente','EvaluacionController@evaluacionesDocente');
    Route::post('getRespuestasGrupo','PregEvaluacionController@getRespuestasGrupo');
    Route::get('getRespuestas/{id}','PregEvaluacionController@getRespuestas');
    Route::post('getRespuestasAcum','PregEvaluacionController@getRespuestasAcum');
    Route::post('evaluacionesEstudianteCurso','EvaluacionController@evaluacionesEstudianteCurso');
    Route::post('evalCompleEstCurso','EvaluacionController@evalCompleEstCurso');
    Route::get('asignaturasDoc/{id}','AsignaturaController@asignaturasDoc');
    Route::get('asignaturasCreaDoc/{id}','AsignaturaController@asignaturasCreaDoc');
    Route::get('verCalificacionEval/{id}','EvaluacionController@verCalificacionEval');
    Route::get('verEvalCursoExport/{id}','EvaluacionController@verEvalCursoExport');
    Route::post('cargarOpcionDico','PreguntaController@cargarOpcionDico');
    Route::post('preguntasxtema','PreguntaController@preguntasxtema');
    Route::post('preguntastipo','PreguntaController@preguntastipo');
    Route::post('preguntasxunidad','PreguntaController@preguntasxunidad');
    Route::post('preguntasevaltipounidad','PreguntaController@preguntasevaltipounidad');
    Route::get('eliminarPregunta/{id}','PreguntaController@eliminarPregunta');
    Route::get('tipospreguntas','PreguntaController@tipospreguntas');
    Route::post('cargarPregsRand','PreguntaController@cargarPregsRand');    
    Route::get('verEstCursoEval/{id}','EvaluacionController@verEstCursoEval');
    Route::post('asignarGrupoEst','EvaluacionController@asignarGrupoEst');
    Route::get('tipoevaluacion', 'EvaluacionController@TiposEvaluacion');
    Route::post('clasifGrupEstEval','PregEvaluacionController@clasifGrupEstEval');    
    Route::post('verRespEstudianteEval', 'PregEvaluacionController@verRespEstudianteEval');
    Route::post('modificarEvaluacion', 'CalificacionEvalController@modificarEvaluacion');
    Route::post('guardarRespuesta','CalificacionEvalController@guardarRespuesta');
    Route::get('eliminar_evaluacion/{id}', 'EvaluacionController@eliminar_evaluacion');
    
    //apis codigos libros
    Route::apiResource('series', 'SeriesController');
    Route::apiResource('libros_series', 'Series_librosController');
    Route::apiResource('codigosLibros', 'CodigosLibrosGenerarController');
    Route::get('codigosLibrosFecha/{id}', 'CodigosLibrosGenerarController@codigosLibrosFecha');
    Route::get('codigosLibrosExportados/{id}', 'CodigosLibrosGenerarController@codigosLibrosExportados');
    Route::get('librosBuscar', 'CodigosLibrosGenerarController@librosBuscar');
    Route::get('codigosLibrosCodigo/{id}','CodigosLibrosGenerarController@codigosLibrosCodigo');
    Route::get('editarCodigoBuscado/{id}','CodigosLibrosGenerarController@editarCodigoBuscado');
    Route::get('estudianteCodigo/{id}','EstudianteController@estudianteCodigo');
    Route::get('cedulasEstudiantes/{id}','EstudianteController@cedulasEstudiantes');
    Route::get('seriesCambiar','CodigosLibrosGenerarController@seriesCambiar');
    Route::get('librosSerieCambiar/{id}','CodigosLibrosGenerarController@librosSerieCambiar');
    Route::get('librosCambiar/{id}','CodigosLibrosGenerarController@librosCambiar');
    Route::post('reportesCodigoInst','CodigosLibrosGenerarController@reportesCodigoInst');
    Route::get('institucionesResportes','CodigosLibrosGenerarController@institucionesResportes');
    Route::post('editarInstEstud', 'CodigosLibrosGenerarController@editarInstEstud');
    Route::get('reportesCodigoAsesor/{id}', 'CodigosLibrosGenerarController@reportesCodigoAsesor');
    Route::get('institucionEstCod/{id}', 'EstudianteController@institucionEstCod');
    

    ///reportes
    Route::get('nivelesInstitucion/{id}', 'NivelController@nivelesInstitucion');
    Route::get('institucionUsuario/{id}', 'usuarioController@institucionUsuario');
    Route::get('docentesInstitucion/{id}','DocenteController@docentesInstitucion');
    Route::get('estudiantesInstitucion/{id}','EstudianteController@estudiantesInstitucion');
    Route::get('reporteLibros','ReporteUsuarioController@index');
    Route::get('docentes','UsuarioController@docentes');
    
    // Estadisticas
    Route::get('contenidos','EstadisticasController@contenidos');

    //JUEGOS
    Route::apiResource('j_juegos', 'J_juegosController');
    Route::get('j_juegosTipos', 'J_juegosController@j_juegosTipos');
    Route::apiResource('j_contenidos', 'J_contenidoController');
    Route::apiResource('tipoJuegos', 'TipoJuegosController');
    Route::get('unidadesAsignatura/{id}', 'TipoJuegosController@unidadesAsignatura');
    Route::get('juego_y_contenido/{id}', 'J_juegosController@juego_y_contenido');
    Route::post('j_juegos_tipo', 'J_juegosController@j_juegos_tipo');
    Route::get('juegos_prolipa_admin_tipo/{id}', 'J_juegosController@juegos_prolipa_admin_tipo');
    Route::post('j_juegos_tipo_prolipa', 'J_juegosController@j_juegos_tipo_prolipa');
    Route::post('j_juegos_ficha', 'J_juegosController@j_juegos_ficha');
    Route::post('guardarTemasJuego', 'J_juegosController@guardarTemasJuego');
    Route::get('eliminarTemasJuego/{id}', 'J_juegosController@eliminarTemasJuego');
    Route::get('j_juegos_eliminar/{id}', 'J_juegosController@j_juegos_eliminar');
    Route::post('j_guardar_calificacion', 'J_juegosController@j_guardar_calificacion');
    Route::post('calificacion_estudiante', 'J_juegosController@calificacion_estudiante');
    Route::post('j_juegos_tipo_curso_doc', 'J_juegosController@j_juegos_tipo_curso_doc');
    Route::post('cursos_jugaron', 'CursoController@cursos_jugaron');
    Route::post('asignar_cursos_juego', 'J_juegosController@asignar_cursos_juego');
    Route::get('juegos_has_curso/{id}', 'J_juegosController@juegos_has_curso');
    Route::post('calificaciones_estudiante_juego', 'J_juegosController@calificaciones_estudiante_juego');


    //ACTIVIDADES - ANIMACIONES
    Route::apiResource('registro_actividades', 'ActividadAnimacionController');
    Route::get('asignaturasActi','ActividadAnimacionController@getAsignaturas');
    Route::get('actividades_x_Tema/{id}', 'ActividadAnimacionController@actividades_x_Tema');
    Route::get('eliminaActividad/{id}', 'ActividadAnimacionController@eliminaActividad');
    Route::post('temasUnidad', 'ActividadAnimacionController@temasUnidad');
    Route::get('temasUnidad_id/{id}', 'ActividadAnimacionController@temasUnidadID');
    Route::get('actividadesBuscarFechas/{id}', 'ActividadAnimacionController@actividadesBuscarFechas');
    Route::get('carpetaActividades/{id}', 'ActividadAnimacionController@carpetaActividades');
    Route::get('actividades_x_Libro/{id}', 'ActividadAnimacionController@actividades_x_Libro');
    Route::get('actividades_libros_unidad/{id}','ActividadAnimacionController@actividades_libros_unidad');
    Route::get('actividades_libros_unidad_tema/{id}','ActividadAnimacionController@actividades_libros_unidad_tema');
    Route::get('animaciones_libros_unidad/{id}','ActividadAnimacionController@animaciones_libros_unidad');
    Route::get('animaciones_libros_unidad_tema/{id}','ActividadAnimacionController@animaciones_libros_unidad_tema');
    
    //VERIFICAR CORREO RESTAURAR CONTRASEÑA
    Route::post('verificarCorreo', 'UsuarioController@verificarCorreo');    
    
    ///CURSOS ADMINISTRADOR
    Route::get('buscarCursoCodigo/{id}', 'CursoController@buscarCursoCodigo');
    Route::post('restaurarCurso/{id}', 'CursoController@restaurarCurso');
    Route::get('cursos_x_usuario/{id}', 'CursoController@cursos_x_usuario');
    Route::get('cursos_x_estudiante/{id}', 'CursoController@cursos_x_estudiante');
    
    //PROMEDIO
    Route::get('cursosInstitucion/{id}', 'ReporteUsuarioController@cursosInstitucion');
    
    //ESTADISTICAS ADMINISTRADOR 
    Route::get('cant_user', 'AdminController@cant_user');
    Route::get('cant_cursos', 'AdminController@cant_cursos');
    Route::get('cant_codigos', 'AdminController@cant_codigos');
    Route::get('cant_codigostotal', 'AdminController@cant_codigostotal');
    Route::get('cant_evaluaciones', 'AdminController@cant_evaluaciones');
    Route::get('cant_preguntas', 'AdminController@cant_preguntas');
    Route::get('cant_multimedia', 'AdminController@cant_multimedia');
    Route::get('cant_juegos', 'AdminController@cant_juegos');
    Route::get('cant_seminarios', 'AdminController@cant_seminarios');
    Route::get('cant_encuestas', 'AdminController@cant_encuestas');
    
    //CANTIDAD EVALUACIONES PERFIL DOCENTE
    Route::get('cant_evaluaciones/{id}', 'DocenteController@cant_evaluaciones');
    
    //CANTIDAD DE ARCHIVOS DE UN DOCENTE
    Route::get('cant_contenido/{id}', 'DocenteController@cant_contenido');
    
    //RUTA DE ENCUESTAS 
    Route::get('encuesta_certificados/{id}', 'SeminarioController@encuesta_certificados');
       
    
    //UNIDADES
    // Route::apiResource('unidadesLibros', 'UnidadController');
    Route::get('libro_enUnidad', 'UnidadController@libro_enUnidad');
    Route::get('unidadesX_Libro/{id}', 'UnidadController@unidadesX_Libro');
    Route::post('updateUnidades', 'UnidadController@updateUnidades');
    
    
    //MATERIAL DE APOYO EN ADMINISTRADOR
    Route::get('todo_asignaturas', 'MaterialApoyoController@todo_asignaturas');
    Route::get('todo_material_apoyo/{id}', 'MaterialApoyoController@todo_material_apoyo');
    Route::get('materialapoyo_asignaturas', 'MaterialApoyoController@materialapoyo_asignaturas');
    Route::post('quitar_material_asignatura', 'MaterialApoyoController@quitar_material_asignatura');
    Route::post('agregar_material_asignaturas', 'MaterialApoyoController@agregar_material_asignaturas');
    Route::post('editar_material_asignaturas', 'MaterialApoyoController@editar_material_asignaturas');
    Route::get('material_estados', 'MaterialApoyoController@material_estados');
    Route::post('registrar_material', 'MaterialApoyoController@registrar_material');
    Route::post('eliminarMaterial', 'MaterialApoyoController@eliminarMaterial');
    Route::get('showMaterial/{id}', 'MaterialApoyoController@showMaterial');
    Route::post('temas_asignatura_material', 'MaterialApoyoController@temas_asignatura_material');
    Route::post('temas_material', 'MaterialApoyoController@temas_material');
    //API OBTENER TEMAS POR MATERIAL
    Route::get('temas_por_material/{id}', 'MaterialApoyoController@temas_por_material');

    // API MATERIASL APOYO DOCENTE
    Route::post('calificaciones_material_curso', 'MaterialApoyoController@calificaciones_material_curso');
    Route::post('material_curso', 'MaterialApoyoController@material_curso');
    Route::post('asignar_cursos_material', 'MaterialApoyoController@asignar_cursos_material');
    Route::post('material_curso_estudiante', 'MaterialApoyoController@material_curso_estudiante');
    Route::post('guardar_material_usuario', 'MaterialApoyoController@guardar_material_usuario');
    
    
    
    //BLOQUEAR - activar CODIGO LIBRO DESDE ADMINISTRADOR
    Route::post('cambioEstadoCodigo', 'CodigosLibrosGenerarController@cambioEstadoCodigo');
    
    //BORRAR TEMAS DE UN MATERIAL
    Route::post('borrar_temas_material', 'MaterialApoyoController@borrar_temas_material');
    Route::post('borrar_material_asig', 'MaterialApoyoController@borrar_material_asig');
    
    Route::post('editar_material', 'MaterialApoyoController@editar_material');
    
    //UNA ASIGNATURA PARA LOS PROYECTOS DEL DOCENTE
    Route::get('asignaturaIdProyectos/{id}', 'ActividadAnimacionController@asignaturaIdProyectos');
    
    //CURSOS POR DOCENTE POR ASIGNATURA SELECCIONADA Y PERIODO LECTIVO ACTIVO
    Route::post('cursos_asignatura_docente', 'CursoController@cursos_asignatura_docente');
    
    //PERIODO LECTIVO ACTIVO PARA REGISTRO DE INSTITUCIONES
    Route::get('periodoActivo', 'PeriodoController@periodoActivo');
    
    //AGREGAR CODIGO LIBRO PERDIDO
    Route::post('agregar_codigo_perdido', 'CodigosLibrosGenerarController@agregar_codigo_perdido');

    //TEMAS TELETAREAS
    Route::get('temas','AsignaturaController@temas');
    Route::get('asigTemas','AsignaturaController@asigTemas');

    //LISTA DE ESTUDIANTES, para historico de visitas
    Route::get('estudiantesXInstitucion/{id}','UsuarioController@estudiantesXInstitucion');
    
    //HISTORICO LIBROS DE ESTUDIANTES
    Route::get('getHistoricoCodigos/{id}','CodigosLibrosGenerarController@getHistoricoCodigos');
    
    //INSTITUCIONES DIRECTOR
    Route::get('institucionesDirector/{id}','PeriodoInstitucionController@institucionesDirector');
    Route::post('guardarLogoInstitucion','InstitucionController@guardarLogoInstitucion');

    // cargar periodo a codigo libro
    Route::get('cargarPeriodoCodigo','CodigosLibrosGenerarController@cargarPeriodoCodigo');

    //seminarios de un docente
    Route::get('seminariosDocente/{id}','SeminarioController@seminariosDocente');


    //salle
    Route::apiResource('areas_salle','SalleAreasController');
    Route::apiResource('asignaturas_salle','SalleAsignaturasController');

    //lista menu 
    Route::get('grupos_users','MenuController@grupos_users');
    Route::get('listaMenu','MenuController@listaMenu');
    Route::post('add_editMenu','MenuController@add_editMenu');
    
    //usuarios salle
    Route::get('usuarioSalle','UsuarioController@usuarioSalle');
    Route::post('add_edit_user_salle','UsuarioController@add_edit_user_salle');
    Route::post('activa_desactiva_user','UsuarioController@activa_desactiva_user');
    Route::post('updatePassword','UsuarioController@cambiarPassword');
    
    //instituciones Salle
    Route::get('institucionesSalle','InstitucionController@institucionesSalle');

     // Apis steven
     Route::resource('libroserie','LibroSerieController');
     //activar o desativar la data de la tabla libro-serie
     Route::post('libroserie/desactivar','LibroSerieController@desactivar');
     Route::post('libroserie/activar','LibroSerieController@activar');
 
     //apis  para la tabla  temporadas
 
     Route::resource('temporadas','TemporadaController')->except(['edit','create']);
     Route::post('temporadas/liquidacion','TemporadaController@liquidacion');
     //api para traer los contratos para que los asesores puedan visualizar
     Route::post('temporadas/asesor/contratos','TemporadaController@asesorcontratos');

     //Api para milton para contratos de liquidacion
     Route::post('temporadas/contrato','TemporadaController@liquidacionApi')->name('temporada.liquidacion');
 
     //api post para testear la api de milton
    Route::post('temporadas/temporadaapi','TemporadaController@generarApiTemporada')->name('temporada.apitemporada');
 
         //apis para testear desde la vista
     Route::get('temporada/crearl','TemporadaController@crearliquidacion');
 
 
    
   
       //activar o desativar la data de la tabla temporada
     Route::post('temporadas/desactivar','TemporadaController@desactivar');
     Route::post('temporadas/activar','TemporadaController@activar');   
     Route::post('temporadas/docente','TemporadaController@agregardocente'); 
});
