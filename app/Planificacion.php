<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planificacion extends Model
{
    protected $table = "planificacion";
    protected $primaryKey = 'idplanificacion';
    protected $fillable = [
        'nombreplanificacion',
        'descripcionplanificacion',
        'webplanificacion',
        'asignatura_idasignatura',
        'Estado_idEstado',
    ];
	public $timestamps = false;
}
