<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temporada extends Model
{
    protected $table = "temporadas";
    protected $primaryKey = 'id_temporada';
    protected $fillable = [
        'contrato',
        'year',
        'temporada',
        'id_profesor',
        'nombre_profesor',
        'id_asesor',
        'nombre_asesor',
        'idInstitucion',
        'nombre_institucion',
        'estado'
       
    ];
}
