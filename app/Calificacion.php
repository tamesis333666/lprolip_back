<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calificacion extends Model
{
    protected $table = "usuario_tarea";
	public $timestamps = false;
}
