<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\SalleAsignaturas;

class SalleAsignaturasController extends Controller
{
    public function index(Request $request)
    {   
        $asignaturas = DB::SELECT("SELECT * FROM salle_asignaturas");

        return $asignaturas;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( $request->id_asignatura ){
            $asignatura = SalleAsignaturas::find($request->id_asignatura);
        }else{
            $asignatura = new SalleAsignaturas();
        }

        $asignatura->nombre_asignatura = $request->nombre_asignatura;
        $asignatura->descripcion_asignatura = $request->descripcion_asignatura;
        $asignatura->unidad = $request->unidad;
        $asignatura->cant_preguntas = $request->cant_preguntas;
        $asignatura->estado = $request->estado;
        $asignatura->save();

        return $asignatura;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $asignaturas = DB::SELECT("SELECT * FROM salle_asignaturas WHERE id_asignatura = $id");

        return $asignaturas;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // $asignatura = SalleAsignaturas::find($request->id);

        // if($asignatura->delete()){
        //     return 1;
        // }else{
        //     return 0;
        // }

    }
}
