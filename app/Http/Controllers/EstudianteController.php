<?php

namespace App\Http\Controllers;

use App\estudiante;
use Illuminate\Http\Request;
use DB;
class EstudianteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = DB::select("CALL `estudiantes` ();");
        return $usuarios;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function estudianteCurso(Request $request){
        $cursos = DB::SELECT("CALL `cursoAlumno` (?);",[$request->idusuario]);
        return $cursos;
    }

    public function cursoSugerencias(){
        $sugerencias = array();
        $aux = array();
        $idusuario = auth()->user()->idusuario;
        $usuarioId = DB::SELECT(" CALL `sugUsuario` (?);",[$idusuario]);
        $cursos = DB::SELECT("SELECT curso.* FROM estudiante join curso on curso.codigo = estudiante.codigo WHERE estudiante.usuario_idusuario = ?",[$idusuario]);
        foreach ($usuarioId as $key => $value) {
            $sugerencia = DB::SELECT("SELECT * FROM curso WHERE curso.idusuario = ?",[$value->idusuario]);
            foreach ($sugerencia as $key => $val) {
                array_push($sugerencias, $val);
            }
        }
        foreach ($cursos as $key => $value) {
            foreach (array_keys($sugerencias, $value) as $key) 
            {
                unset($sugerencias[$key]);
            }
        }
        $aux = array_values($sugerencias);
        return $aux;
    }

    public function remover ($valor,$arr)
    {
        foreach (array_keys($arr, $valor) as $key) 
        {
            unset($arr[$key]);
        }
        return $arr;
    }

    public function tareasEstudiante($id){
        $tareas = DB::SELECT('SELECT * FROM tarea WHERE tarea.curso_idcurso = ?', [$id]);
        return $tareas;
    }

    public function tareaEstudiantePendiente(Request $request){
        $data=array();
        $tarea = DB::SELECT("SELECT * FROM tarea left join contenido on contenido.idcontenido = tarea.contenido_idcontenido WHERE tarea.curso_idcurso = ? AND tarea.estado = '1' AND tarea.usuario_idusuario IS NULL OR tarea.usuario_idusuario = ?",[$request->idcurso,$request->idusuario]);
        foreach ($tarea as $key => $post) {
            $verifica = DB::SELECT("SELECT * FROM usuario_tarea WHERE tarea_idtarea = ? AND usuario_idusuario = ?",[$post->idtarea,$request->idusuario]);
            if(!empty($verifica)){
            }else{
                array_push ($data , $post);
            }
        }
        return $data;
    }

    public function tareaEstudianteRealizada(Request $request){
        $data=array();
        $tarea = DB::SELECT("SELECT * FROM tarea WHERE curso_idcurso = ? AND estado = '1' ",[$request->idcurso]);
        foreach ($tarea as $key => $post) {            
            $verifica = DB::SELECT("SELECT tarea.*,usuario_tarea.*,contenido.url as urldocente,contenido.nombre as tareadocente FROM usuario_tarea join tarea on tarea.idtarea = usuario_tarea.tarea_idtarea left join contenido on contenido.idcontenido = tarea.contenido_idcontenido WHERE tarea_idtarea = ? AND usuario_tarea.usuario_idusuario = ?",[$post->idtarea,$request->idusuario]);

            if(!empty($verifica)){
                array_push ($data , $verifica[0]);
            }else{
            }
        }
        return $data;
    }

 
    public function estudianteCodigo($id){
        $estudiante = DB::SELECT("SELECT u.cedula, u.nombres, u.apellidos, u.email FROM usuario u WHERE idusuario = $id");

        return $estudiante;
    }


    
    public function cedulasEstudiantes($cedula){
        $cedula = DB::SELECT("SELECT u.idusuario, u.cedula, u.nombres, u.apellidos, u.email FROM usuario u WHERE u.id_group = 4 AND u.cedula like '%$cedula%'");

        return $cedula;
        
    }

    
    public function institucionEstCod($id){
        $institucion = DB::SELECT("SELECT idInstitucion, nombreInstitucion, ciudad.nombre as nombre_ciudad, pi.periodoescolar_idperiodoescolar as id_periodo FROM institucion, usuario, ciudad, periodoescolar_has_institucion pi WHERE usuario.institucion_idInstitucion = institucion.idInstitucion AND ciudad.idciudad = institucion.ciudad_id AND usuario.idUsuario = $id AND usuario.institucion_idInstitucion = pi.institucion_idInstitucion AND pi.id = (SELECT MAX(phi.id) AS periodo_maximo FROM periodoescolar_has_institucion phi WHERE phi.institucion_idInstitucion = institucion.idInstitucion)");

        return $institucion;
        
    }


    public function addClase(Request $request){
        $datosValidados=$request->validate([
            'codigo' => 'exists:curso,codigo',
        ]);
        $idusuario = $request->idusuario;
        $codigo = $request->codigo;
        DB::INSERT('INSERT INTO `estudiante`(`usuario_idusuario`, `codigo`) VALUES (?,?)',[$idusuario,$codigo]);
    }


    public function verificarCursoEstudiante(Request $request){
        
        $estudiante = DB::SELECT("SELECT * FROM estudiante WHERE usuario_idusuario = $request->idusuario AND codigo = '$request->codigo'");
        return $estudiante;
    }

    
    public function estudiantesEvalCurso(Request $request)
    {
        $estudiantes = DB::SELECT("SELECT DISTINCT u.idusuario as id, CONCAT(u.nombres, ' ', u.apellidos) as label, c.grupo FROM estudiante e, usuario u, calificaciones c WHERE e.codigo = '$request->codigo' AND e.usuario_idusuario = u.idusuario AND c.id_estudiante = e.usuario_idusuario AND c.id_evaluacion = $request->evaluacion AND e.usuario_idusuario IN (SELECT c.id_estudiante FROM calificaciones c WHERE c.id_evaluacion = $request->evaluacion) ORDER BY u.nombres");

        return $estudiantes;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\estudiante  $estudiante
     * @return \Illuminate\Http\Response
     */
    public function show($cedula)
    {
        $estudiante = DB::SELECT("SELECT idusuario FROM usuario WHERE cedula = $cedula");

        return $estudiante;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\estudiante  $estudiante
     * @return \Illuminate\Http\Response
     */
    public function edit(estudiante $estudiante)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\estudiante  $estudiante
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, estudiante $estudiante)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\estudiante  $estudiante
     * @return \Illuminate\Http\Response
     */
    public function destroy(estudiante $estudiante)
    {
        //
    }
}
