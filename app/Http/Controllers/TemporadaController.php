<?php

namespace App\Http\Controllers;

use App\Temporada;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Usuario;
use App\Institucion;
use Illuminate\Support\Facades\DB;


class TemporadaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function crearliquidacion(Request $request){
        return view('testearapis.apitemporada');
    }


    public function index(Request $request)
    {
        // $asesores =  DB::table('usuario')
        //         ->where('id_group', '11')
        //         ->where('estado_idEstado','1')
        //         ->get();


        
        $asesores= DB::table('usuario')
            ->select(DB::raw('CONCAT(usuario.nombres , " " , usuario.apellidos ) as asesornombres'),'usuario.idusuario','usuario.nombres','usuario.cedula')
            ->where('id_group', '11')
            ->where('estado_idEstado','1')
            ->get();
        
         $profesores= DB::table('usuario')
            ->select(DB::raw('CONCAT(usuario.nombres , " " , usuario.apellidos ) as  profesornombres'),'usuario.idusuario','usuario.nombres','usuario.cedula')
            ->where('id_group', '6')
            ->where('estado_idEstado','1')
            ->get();
        
       


        $institucion = Institucion::where('estado_idEstado', '=',1)->get();
      
             // $temporada = DB::select("select t.* ,i.idInstitucion , i.nombreInstitucion, p.nombres as profesor, a.nombres as asesor, a.idusuario , a.nombres , a.apellidos, p.idusuario  as idusuarios, p.apellidos as papellido, p.cedula as pcedula
      //       from temporadas t,institucion  i, usuario p , usuario a
      //       where t.idInstitucion  = i.idInstitucion 
      //       and t.id_profesor = p.idusuario 
      //       and t.id_asesor = a.idusuario 
           

      //    "); 

         $temporada = DB::select("select t.* 
            from temporadas t 
           

         "); 
       
          return ['temporada' => $temporada, 'asesores'=> $asesores,'profesores' => $profesores, 'listainstitucion' => $institucion];

    }


   



    public function store(Request $request)
    {
         if( $request->id ){
            $temporada = Temporada::find($request->id);
            $temporada->contrato = $request->contrato;
            $temporada->year = $request->year;
            $temporada->ciudad = $request->ciudad;
            $temporada->temporada = $request->temporada;
            $temporada->id_asesor = $request->id_asesor;
            $temporada->cedula_asesor = $request->cedula_asesor;
            $temporada->id_profesor = $request->id_profesor;
            $temporada->idInstitucion  = $request->idInstitucion;
    
        }else{
            $temporada = new Temporada();
            $temporada->contrato = $request->contrato;
            $temporada->year = $request->year;
            $temporada->ciudad = $request->ciudad;
            $temporada->temporada = $request->temporada;
            $temporada->id_profesor = $request->id_profesor;
            $temporada->temporal_cedula_docente = $request->temporal_cedula_docente;
            $temporada->temporal_nombre_docente = $request->temporal_nombre_docente;
            $temporada->idInstitucion  = $request->idInstitucion;
            $temporada->temporal_institucion  = $request->temporal_institucion;
            $temporada->id_asesor = $request->id_asesor;
            $temporada->cedula_asesor = $request->cedula_asesor;
            $temporada->nombre_asesor = $request->nombre_asesor;
        }


        
       
        $temporada->save();

        return $temporada;
    }
    //api para que los asesores puedan ver sus contratos
    public function asesorcontratos(Request $request){
        $cedula = $request->cedula;

          
        $temporadas= DB::table('temporadas')
            ->select('temporadas.*')
            ->where('cedula_asesor', $cedula)
            ->where('estado','1')
            ->get();

        return $temporadas;
        
    }

 

    public function liquidacion(Request $request){

      $temporada = Temporada::where('id_temporada',$request->id_Temporada)->get();

      $codigos_libros = DB::SELECT("(SELECT COUNT(c.codigo) as cantidad, GROUP_CONCAT(DISTINCT c.serie) as serie, (SELECT l.nombrelibro FROM libro l WHERE l.idlibro = (GROUP_CONCAT(DISTINCT c.libro_idlibro)) ) as libro, GROUP_CONCAT(DISTINCT (SELECT ciudad.nombre FROM ciudad WHERE ciudad.idciudad = i.ciudad_id) ) as ciudad, GROUP_CONCAT(DISTINCT (SELECT GROUP_CONCAT(usuario.nombres,' ',usuario.apellidos) as vendedor FROM usuario WHERE usuario.cedula = (SELECT institucion.vendedorInstitucion FROM institucion WHERE institucion.idInstitucion = i.idInstitucion ) ) ) as asesor, GROUP_CONCAT(DISTINCT (SELECT i.nombreInstitucion FROM institucion WHERE institucion.idInstitucion = i.idInstitucion ) ) as institucion FROM codigoslibros c, usuario u, institucion i WHERE c.idusuario = u.idusuario AND u.institucion_idInstitucion = i.idInstitucion AND i.idInstitucion = $request->id AND c.updated_at BETWEEN CAST('$request->fromdate' AS DATE) AND CAST('$request->todate' AS DATE) AND c.codigo not like '%plus%' GROUP BY c.libro_idlibro ORDER BY `c`.`updated_at`  DESC) UNION (SELECT COUNT(c.codigo) as cantidad, GROUP_CONCAT(DISTINCT c.serie, ' PLUS') as serie, (SELECT l.nombrelibro FROM libro l WHERE l.idlibro = (GROUP_CONCAT(DISTINCT c.libro_idlibro)) ) as libro, GROUP_CONCAT(DISTINCT (SELECT ciudad.nombre FROM ciudad WHERE ciudad.idciudad = i.ciudad_id) ) as ciudad, GROUP_CONCAT(DISTINCT (SELECT GROUP_CONCAT(usuario.nombres,' ',usuario.apellidos) as vendedor FROM usuario WHERE usuario.cedula = (SELECT institucion.vendedorInstitucion FROM institucion WHERE institucion.idInstitucion = i.idInstitucion ) ) ) as asesor, GROUP_CONCAT(DISTINCT (SELECT i.nombreInstitucion FROM institucion WHERE institucion.idInstitucion = i.idInstitucion ) ) as institucion FROM codigoslibros c, usuario u, institucion i WHERE c.idusuario = u.idusuario AND u.institucion_idInstitucion = i.idInstitucion AND i.idInstitucion = $request->id AND c.updated_at BETWEEN CAST('$request->fromdate' AS DATE) AND CAST('$request->todate' AS DATE) AND c.codigo like '%plus%' GROUP BY c.libro_idlibro ORDER BY `c`.`updated_at`  DESC)");



            return ['temporada'=>$temporada,'codigos_libros' => $codigos_libros];



    }


    //api para MILTON
    public function liquidacionApi(Request $request){

      $temporada = Temporada::where('contrato',$request->contrato)->get();
      // $institucion_nombre = 'UNIDAD EDUCATIVA PARTICULAR JOHN BELLERS';
      $buscarinstitucion = "UNIDAD EDUCATIVA PARTICULAR JOHN BELLERS";
      // $institucion_nombre = $request->get('institucion');
        // $temporada = DB::select("select t.* ,i.idInstitucion , i.nombreInstitucion, p.nombres as profesor, a.nombres as asesor, a.idusuario , a.nombres , a.apellidos, p.idusuario  as idusuarios, p.apellidos as papellido, p.cedula as pcedula
        //     from temporadas t,institucion  i, usuario p , usuario a
        //     where t.idInstitucion  = i.idInstitucion 
        //     and t.id_profesor = p.idusuario 
        //     and t.id_asesor = a.idusuario 
        //     and t.nombre_asesor LIKE '%$buscar%'

        //  ");



 $codigos_libros = DB::SELECT("(SELECT COUNT(c.codigo) as cantidad, GROUP_CONCAT(DISTINCT c.serie) as serie, (SELECT l.nombrelibro FROM libro l WHERE l.idlibro = (GROUP_CONCAT(DISTINCT c.libro_idlibro)) ) as libro, GROUP_CONCAT(DISTINCT (SELECT ciudad.nombre FROM ciudad WHERE ciudad.idciudad = i.ciudad_id) ) as ciudad, GROUP_CONCAT(DISTINCT (SELECT GROUP_CONCAT(usuario.nombres,' ',usuario.apellidos) as vendedor FROM usuario WHERE usuario.cedula = (SELECT institucion.vendedorInstitucion FROM institucion WHERE institucion.idInstitucion = i.idInstitucion ) ) ) as asesor, GROUP_CONCAT(DISTINCT (SELECT i.nombreInstitucion FROM institucion WHERE institucion.idInstitucion = i.idInstitucion ) ) as institucion FROM codigoslibros c, usuario u, institucion i WHERE c.idusuario = u.idusuario AND u.institucion_idInstitucion = i.idInstitucion AND i.nombreInstitucion  = '$buscarinstitucion' AND c.updated_at BETWEEN CAST('$request->fromdate' AS DATE) AND CAST('$request->todate' AS DATE) AND c.codigo not like '%plus%' GROUP BY c.libro_idlibro ORDER BY `c`.`updated_at`  DESC) UNION (SELECT COUNT(c.codigo) as cantidad, GROUP_CONCAT(DISTINCT c.serie, ' PLUS') as serie, (SELECT l.nombrelibro FROM libro l WHERE l.idlibro = (GROUP_CONCAT(DISTINCT c.libro_idlibro)) ) as libro, GROUP_CONCAT(DISTINCT (SELECT ciudad.nombre FROM ciudad WHERE ciudad.idciudad = i.ciudad_id) ) as ciudad, GROUP_CONCAT(DISTINCT (SELECT GROUP_CONCAT(usuario.nombres,' ',usuario.apellidos) as vendedor FROM usuario WHERE usuario.cedula = (SELECT institucion.vendedorInstitucion FROM institucion WHERE institucion.idInstitucion = i.idInstitucion ) ) ) as asesor, GROUP_CONCAT(DISTINCT (SELECT i.nombreInstitucion FROM institucion WHERE institucion.idInstitucion = i.idInstitucion ) ) as institucion FROM codigoslibros c, usuario u, institucion i WHERE c.idusuario = u.idusuario AND u.institucion_idInstitucion = i.idInstitucion AND i.nombreInstitucion  = '$buscarinstitucion' AND c.updated_at BETWEEN CAST('$request->fromdate' AS DATE) AND CAST('$request->todate' AS DATE) AND c.codigo like '%plus%' GROUP BY c.libro_idlibro ORDER BY `c`.`updated_at`  DESC)");



            return ['temporada'=>$temporada,'codigos_libros' => $codigos_libros];



    }

    //Api para milton para nos envia la data y nos guarde en nuestra bd
    public function generarApiTemporada(Request $request){
        $temporada = new Temporada();
        $temporada->contrato = $request->contrato; 
        $temporada->year = $request->year; 
        $temporada->ciudad = $request->ciudad; 
        $temporada->temporada = $request->temporada; 
        $temporada->temporal_nombre_docente = $request->temporal_nombre_docente; 
        $temporada->temporal_cedula_docente = $request->temporal_cedula_docente; 
        $temporada->temporal_institucion = $request->temporal_institucion; 
        $temporada->nombre_asesor = $request->nombre_asesor;
        //campos a null
        $temporada->id_profesor= "0";
        $temporada->id_profesor= "0";
        $temporada->id_asesor= "0";
        $temporada->idInstitucion= "0";
        $temporada->cedula_asesor = "0";
        $temporada->save();

        return response()->json($temporada);
    }

   
    

     public function desactivar(Request $request)
    {
        
        $temporada =  Temporada::findOrFail($request->get('id_temporada'));
        
        $temporada->estado = 0;
        $temporada->save();
        return response()->json($temporada);
    }

     public function activar(Request $request)
    {
       
        $temporada =  Temporada::findOrFail($request->get('id_temporada'));
        
        $temporada->estado = 1;
        $temporada->save();
        return response()->json($temporada);
    }
    // funcion para agregar el docente a la vista de temporadas
    public function agregardocente(Request $request){
        $docente = new Usuario();
        $docente->cedula = $request->cedula;
        $docente->nombres = $request->nombres;
        $docente->apellidos = $request->apellidos;
        $docente->email = $request->email;
        $docente->name_usuario = $request->name_usuario;
        $docente->password=sha1(md5($request->cedula));
        $docente->id_group = 6;
        $docente->institucion_idInstitucion  = $request->institucion_idInstitucion;
        $docente->save();

        return $docente;
    }
}
