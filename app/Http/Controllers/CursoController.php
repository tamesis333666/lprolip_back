<?php

namespace App\Http\Controllers;

use App\Curso;
use Illuminate\Http\Request;
use Dirape\Token\Token;
use DB;
use DateTime;
use Illuminate\Support\Facades\Storage;
class CursoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $curso = DB::select("SELECT DISTINCT c. * FROM curso c, institucion_has_usuario iu, periodoescolar_has_institucion phi, periodoescolar p WHERE c.idusuario =  $request->idusuario AND c.estado = '1' AND c.idusuario = iu.usuario_idusuario AND iu.institucion_idInstitucion = phi.institucion_idInstitucion AND phi.periodoescolar_idperiodoescolar = p.idperiodoescolar AND p.estado = '1'");

        return $curso;
    }

    
    public function cursos_evaluaciones($id_usuario)
    {
        // $curso = DB::select("SELECT DISTINCT c. *, a.nombreasignatura, a.area_idarea, a.idasignatura, a.nivel_idnivel, a.tipo_asignatura FROM curso c, asignatura a, institucion_has_usuario iu, periodoescolar_has_institucion phi, periodoescolar p WHERE c.idusuario = $id_usuario AND c.estado = '1' AND c.idusuario = iu.usuario_idusuario AND iu.institucion_idInstitucion = phi.institucion_idInstitucion AND phi.periodoescolar_idperiodoescolar = p.idperiodoescolar AND p.estado = '1' AND c.id_asignatura = a.idasignatura");
        
        $cursos = DB::SELECT("SELECT DISTINCT c. *, a.nombreasignatura, a.area_idarea, a.idasignatura, a.nivel_idnivel, a.tipo_asignatura FROM curso c, asignatura a, usuario u, periodoescolar_has_institucion phi, periodoescolar p WHERE c.idusuario = u.idusuario AND c.idusuario = $id_usuario AND c.estado = '1' AND u.institucion_idInstitucion = phi.institucion_idInstitucion AND phi.periodoescolar_idperiodoescolar = p.idperiodoescolar AND p.estado = '1' AND c.id_asignatura = a.idasignatura");

        return $cursos;
    }



    public function curso_asig_docente($id)
    {
        $curso = DB::select("SELECT * FROM curso c WHERE c.id_asignatura = $id AND c.estado = '1'");

        return $curso;
    }

    
    public function cursos_jugaron(Request $request)
    {
        $codigos = DB::SELECT("SELECT c.codigo, c.nombre, c.seccion, c.materia, c.aula FROM curso c WHERE c.idusuario = $request->id_docente");
        if(!empty($codigos)){
            foreach ($codigos as $key => $value) {
                $calificaciones = DB::SELECT("SELECT COUNT(*) as cantidad FROM j_calificaciones jc, estudiante e WHERE jc.id_juego = ? AND jc.id_usuario = e.usuario_idusuario AND e.codigo = ?",[$request->id_juego, $value->codigo]);

                $data['items'][$key] = [
                    'codigo' => $value->codigo,
                    'nombre' => $value->nombre,
                    'seccion' => $value->seccion,
                    'materia' => $value->materia,
                    'aula' => $value->aula,
                    'calificaciones'=>$calificaciones,
                ];            
            }
        }else{
            $data = [];
        }
        return $data;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // $curso = Curso::create([
        //     'nombre' => $request->nombre,
        //     'seccion' => $request->seccion,
        //     'materia' => $request->materia,
        //     'aula' => $request->aula,
        //     'codigo' => (new Token())->Unique('curso', 'codigo', 8),
        //     'idusuario'=> auth()->user()->idusuario
        // ]);
        // return $curso;
    }

    public function store(Request $request)
    {
        if(empty($request->idcurso)){
            $curso = Curso::create([
                'nombre' => $request->nombre,
                'id_asignatura'=> $request->id_asignatura,
                'seccion' => $request->seccion,
                'materia' => $request->materia,
                'aula' => $request->aula,
                'codigo' => $this->codigo(8),
                'idusuario'=> $request->idusuario
            ]);
        }else{
            $curso=DB::update("UPDATE curso SET nombre=?,seccion=?,materia=?,aula=?, id_asignatura=? WHERE idcurso=?",[$request->nombre,$request->seccion,$request->materia,$request->aula,$request->id_asignatura,$request->idcurso]);
        }
        return $curso;
    }

    function codigo($count)
    {
        // This string MUST stay FS safe, avoid special chars
        $base = 'ABCDEFGHKMNPRSTUVWXYZ123456789';
        $ret = '';
        $strlen = \strlen($base);
        for ($i = 0; $i < $count; ++$i) {
            $ret .= $base[random_int(0, $strlen - 1)];
        }

        return $ret;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function show(Curso $curso)
    {
        return $curso;
    }

    
    public function curso_libro_docente(Request $request){
        // $cursos = DB::SELECT("SELECT DISTINCT c. * FROM curso c, institucion_has_usuario iu, periodoescolar_has_institucion phi, periodoescolar p WHERE c.idusuario =  $request->id_usuario AND c.id_asignatura = $request->id_asignatura AND c.estado = '1' AND c.idusuario = iu.usuario_idusuario AND iu.institucion_idInstitucion = phi.institucion_idInstitucion AND phi.periodoescolar_idperiodoescolar = p.idperiodoescolar AND p.estado = '1'");

        $cursos = DB::SELECT("SELECT DISTINCT c. * FROM curso c, usuario u, periodoescolar_has_institucion phi, periodoescolar p WHERE c.idusuario = u.idusuario AND c.idusuario = $request->id_usuario AND c.id_asignatura = $request->id_asignatura AND c.estado = '1' AND u.institucion_idInstitucion = phi.institucion_idInstitucion AND phi.periodoescolar_idperiodoescolar = p.idperiodoescolar AND p.estado = '1'");

        return $cursos;
    }

    public function addTareaContenido(Request $request){
        $idusuario = $request->idusuario;
        $comentario = $request->comentario_estudiante;
        $file = $request->file('archivo');
        $ruta = '/var/www/vhosts/prolipadigital.com.ec/httpdocs/software/PlataformaProlipa/public';
        $fileName = uniqid().$file->getClientOriginalName();
        $file->move($ruta,$fileName);
        $request->session()->flash('notificacion','Archivo Subido');
        DB::INSERT("INSERT INTO usuario_tarea(nombre, url,tarea_idtarea,curso_idcurso,usuario_idusuario,comentario_estudiante) VALUES (?,?,?,?,?,?)",[$file->getClientOriginalName(),$fileName,$request->idtarea,$request->idcurso,$idusuario,$comentario]);
    }

    public function addContenido(Request $request){
        $file = $request->file('archivo');
        $ruta = '/var/www/vhosts/prolipadigital.com.ec/httpdocs/software/PlataformaProlipa/public';
        $fileName = uniqid().$file->getClientOriginalName();
        $file->move($ruta,$fileName);
        DB::INSERT("INSERT INTO contenido(nombre, url, curso_idcurso) VALUES (?,?,?)",[$file->getClientOriginalName(),$fileName,$request->idcurso]);
    }

    public function getContenido(Request $request){
        $date = new DateTime();
        $fecha = $date->format('y-m-d');
        if($request->idasignatura != 'null' ){
            $asig = DB::SELECT("SELECT *
            FROM contenido
            WHERE contenido.idasignatura = ? AND contenido.estado = '1'
            ",[$request->idasignatura] );
        }else{
            $asig = DB::SELECT("SELECT *
            FROM contenido
            WHERE contenido.curso_idcurso = ? AND contenido.estado = '1'
            ",[$request->idcurso] );
        }
        if(empty($request->idasignatura)){
            $asig = DB::SELECT("SELECT *
                FROM contenido
                WHERE contenido.curso_idcurso = ? AND contenido.estado = '1'
                ",[$request->idcurso] );
        }
        return $asig;
    }

    public function getContenidoTodo(Request $request){
        if(empty($request->idasignatura) && empty($request->unidad) ){
            $asig = DB::SELECT("SELECT contenido.*
            FROM asignaturausuario
            JOIN contenido ON asignaturausuario.asignatura_idasignatura = contenido.idasignatura
            WHERE asignaturausuario.usuario_idusuario = ?
            ",[$request->idusuario] );
            foreach ($asig as $key => $post) {
                try {
                    $respuesta = DB::SELECT("SELECT temas.nombre_tema FROM temas_has_contenido JOIN temas ON temas.id = temas_has_contenido.temas_id WHERE temas_has_contenido.contenido_idcontenido = ? ",[$post->idcontenido]);
                    $data['items'][$key] = [
                        'idcontenido' => $post->idcontenido,
                        'nombre' => $post->nombre,
                        'url' => $post->url,
                        'unidad' => $post->unidad,
                        'updated_at' => $post->updated_at,
                        'idasignatura' => $post->idasignatura,
                        'temas'=>$respuesta,
                    ];
                } catch (\Throwable $th) {
                    $data['items'][$key] = [
                        'idcontenido' => $post->idcontenido,
                        'nombre' => $post->nombre,
                        'url' => $post->url,
                        'unidad' => $post->unidad,
                        'updated_at' => $post->updated_at,
                        'idasignatura' => $post->idasignatura,
                        'temas'=>[],
                    ];
                }
            }

        }else{
            if (empty($request->idasignatura) || $request->idasignatura == 'null') {
                $asig = DB::SELECT("SELECT contenido.*
                FROM asignaturausuario
                JOIN contenido ON asignaturausuario.asignatura_idasignatura = contenido.idasignatura
                WHERE asignaturausuario.usuario_idusuario = ? AND contenido.unidad = ?
                ",[$request->idusuario,$request->unidad] );
                foreach ($asig as $key => $post) {
                try {
                    $respuesta = DB::SELECT("SELECT temas.nombre_tema FROM temas_has_contenido JOIN temas ON temas.id = temas_has_contenido.temas_id WHERE temas_has_contenido.contenido_idcontenido = ? ",[$post->idcontenido]);
                    $data['items'][$key] = [
                        'idcontenido' => $post->idcontenido,
                        'nombre' => $post->nombre,
                        'url' => $post->url,
                        'unidad' => $post->unidad,
                        'updated_at' => $post->updated_at,
                        'idasignatura' => $post->idasignatura,
                        'temas'=>$respuesta,
                    ];
                } catch (\Throwable $th) {
                    $data['items'][$key] = [
                        'idcontenido' => $post->idcontenido,
                        'nombre' => $post->nombre,
                        'url' => $post->url,
                        'unidad' => $post->unidad,
                        'updated_at' => $post->updated_at,
                        'idasignatura' => $post->idasignatura,
                        'temas'=>[],
                    ];
                }
            }

                
            }else{
                if (empty($request->unidad)) {
                    $asig = DB::SELECT("SELECT contenido.*
                    FROM asignaturausuario
                    JOIN contenido ON asignaturausuario.asignatura_idasignatura = contenido.idasignatura
                    WHERE asignaturausuario.usuario_idusuario = ? AND contenido.idasignatura = ?
                    ",[$request->idusuario,$request->idasignatura] );
                    foreach ($asig as $key => $post) {
                try {
                    $respuesta = DB::SELECT("SELECT temas.nombre_tema FROM temas_has_contenido JOIN temas ON temas.id = temas_has_contenido.temas_id WHERE temas_has_contenido.contenido_idcontenido = ? ",[$post->idcontenido]);
                    $data['items'][$key] = [
                        'idcontenido' => $post->idcontenido,
                        'nombre' => $post->nombre,
                        'url' => $post->url,
                        'unidad' => $post->unidad,
                        'updated_at' => $post->updated_at,
                        'idasignatura' => $post->idasignatura,
                        'temas'=>$respuesta,
                    ];
                } catch (\Throwable $th) {
                    $data['items'][$key] = [
                        'idcontenido' => $post->idcontenido,
                        'nombre' => $post->nombre,
                        'url' => $post->url,
                        'unidad' => $post->unidad,
                        'updated_at' => $post->updated_at,
                        'idasignatura' => $post->idasignatura,
                        'temas'=>[],
                    ];
                }
            }

                    
                } else {
                    $asig = DB::SELECT("SELECT contenido.*
                    FROM asignaturausuario
                    JOIN contenido ON asignaturausuario.asignatura_idasignatura = contenido.idasignatura
                    WHERE asignaturausuario.usuario_idusuario = ? AND contenido.idasignatura = ? AND contenido.unidad = ?
                    ",[$request->idusuario,$request->idasignatura,$request->unidad] );
                    foreach ($asig as $key => $post) {
                try {
                    $respuesta = DB::SELECT("SELECT temas.nombre_tema FROM temas_has_contenido JOIN temas ON temas.id = temas_has_contenido.temas_id WHERE temas_has_contenido.contenido_idcontenido = ? ",[$post->idcontenido]);
                    $data['items'][$key] = [
                        'idcontenido' => $post->idcontenido,
                        'nombre' => $post->nombre,
                        'url' => $post->url,
                        'unidad' => $post->unidad,
                        'updated_at' => $post->updated_at,
                        'idasignatura' => $post->idasignatura,
                        'temas'=>$respuesta,
                    ];
                } catch (\Throwable $th) {
                    $data['items'][$key] = [
                        'idcontenido' => $post->idcontenido,
                        'nombre' => $post->nombre,
                        'url' => $post->url,
                        'unidad' => $post->unidad,
                        'updated_at' => $post->updated_at,
                        'idasignatura' => $post->idasignatura,
                        'temas'=>[],
                    ];
                }
            }

                    
                }
            }

        }
        return $data;
    }

    public function getEstudiantes(Request $request){
        $contenido = DB::SELECT("CALL estudianteCurso (?);",["$request->codigo"]);
        return $contenido;
    }

    public function Calificacion(Request $request){
        $estudiantes = DB::SELECT("CALL `estudianteCurso`(?);",["$request->codigo"]);
        if(!empty($estudiantes)){
            foreach ($estudiantes as $key => $value) {
                $total = DB::SELECT("SELECT * FROM tarea  WHERE tarea.curso_idcurso = ? AND tarea.estado = '1' ",[$value->idcurso]);
                $tareas = DB::SELECT("SELECT usuario_tarea.nota  FROM tarea join usuario_tarea on usuario_tarea.tarea_idtarea = tarea.idtarea WHERE tarea.curso_idcurso = ? and usuario_tarea.usuario_idusuario = ? ",[$value->idcurso,$value->idusuario]);
                $data['items'][$key] = [
                    'idusuario' => $value->idusuario,
                    'cedula' => $value->cedula,
                    'foto_user' => $value->foto_user,
                    'nombres' => $value->nombres,
                    'apellidos' => $value->apellidos,
                    'tareas'=>$this->TareasCalificadas($value->idcurso,$value->idusuario),
                    'total'=>$total,
                ];            
            }
        }else{
            $data = [];
        }
        return $data;
    }

    public function TareasCalificadas($idcurso,$idusuario){
        $nota = '0';
        $notas=[];
        $total = DB::SELECT("SELECT * FROM tarea  WHERE tarea.curso_idcurso = ? AND tarea.estado = '1' ",[$idcurso]);
        foreach ($total as $key => $value) {
            $tareas = DB::SELECT("SELECT usuario_tarea.nota  FROM tarea join usuario_tarea on usuario_tarea.tarea_idtarea = tarea.idtarea WHERE usuario_tarea.tarea_idtarea = ? and usuario_tarea.usuario_idusuario = ? ",[$value->idtarea,$idusuario]);
            if(!empty($tareas)){
                foreach ($tareas as $key => $value) {
                    $nota = [
                        'nota'=> $value->nota
                    ];
                }
            }else{
                $nota = [
                    'nota'=> 0
                ];
            }
            array_push($notas, $nota);
        }
        return $notas;

    } 

    

    public function eliminarContenido(Request $request){
        DB::UPDATE("UPDATE `contenido` SET `estado`='0' WHERE `idcontenido`=?",[$request->id]);
    }

    public function guardarTarea(Request $request){

        $idEst = explode(",", $request->idestudiantes);
        $tam = sizeof($idEst);

        if(empty($request->idtarea)){
            if($request->idestudiantes == '' ){
                DB::INSERT("INSERT INTO tarea(fecha_inicio, fecha_final, descripcion, contenido_idcontenido, curso_idcurso) VALUES (?,?,?,?,?)",[$request->finicial,$request->ffinal,$request->descripcion,$request->idcontenido,$request->idcurso]);
            }else{
                for( $i=0; $i<$tam; $i++ ){
                    $idusuarioEst = $idEst[$i];
                    DB::INSERT("INSERT INTO tarea(fecha_inicio, fecha_final, descripcion, contenido_idcontenido, curso_idcurso, usuario_idusuario) VALUES (?,?,?,?,?,?)",[$request->finicial,$request->ffinal,$request->descripcion,$request->idcontenido,$request->idcurso,$idusuarioEst]);
                }
            }
        }else{
            $tarea = DB::UPDATE("UPDATE `tarea` SET `fecha_inicio`=?,`fecha_final`=?,`descripcion`=?,`contenido_idcontenido`=?,`curso_idcurso`=? WHERE `idtarea` = ?",[$request->finicial,$request->ffinal,$request->descripcion,$request->idcontenido,$request->idcurso,$request->idtarea]);
            
            return $tarea;
        }
        //return $idEst[1];
    }

    public function librosCurso(Request $request){
        $libro = DB::SELECT("SELECT libro_has_curso.id_libro_has_curso as id ,libro.* FROM libro_has_curso join libro on libro.idlibro = libro_has_curso.libro_idlibro   WHERE curso_idcurso = ? AND libro_has_curso.estado = '1' ORDER BY id desc ",[$request->idcurso]);
        return $libro;
    }

    public function librosCursoEliminar(Request $request){
        DB::UPDATE("UPDATE libro_has_curso SET estado='0' WHERE id_libro_has_curso = ?",[$request->id]);
    }

    public function getTareas(Request $request){
        $idusuario = auth()->user()->idusuario;
        if(empty($request->fecha)){
            $tareas = DB::SELECT("CALL getTareas (?);",[$request->idcurso]);
        }else{
            $tareas = DB::SELECT("CALL getTareasFecha (?,?);",[$request->idcurso,$request->fecha]);
        }
        if(!empty($tareas)){
            foreach ($tareas as $key => $post) {
                $respuesta = DB::SELECT("SELECT * FROM usuario_tarea WHERE tarea_idtarea = ? && usuario_idusuario = ? ",[$post->idtarea,$idusuario]);
                $total = DB::SELECT("SELECT count(*) as cantidad FROM usuario_tarea WHERE tarea_idtarea = ? && usuario_idusuario = ? ",[$post->idtarea,$idusuario]);
                $data['items'][$key] = [
                    'tarea' => $post,
                    'total'=>$total,
                    'respuesta'=>$respuesta,
                ];
            }
            return $data;
        }else{
            $data = [];
            return $data;
        }
    }

    public function postCalificacion(Request $request){
        DB::UPDATE("UPDATE usuario_tarea set nota=?, observacion=? WHERE id=?",[$request->nota,$request->observacion,$request->id]);
        $respuesta = DB::SELECT("SELECT * FROM usuario_tarea join usuario on usuario_idusuario = idusuario  WHERE tarea_idtarea = ? ",[$request->idtarea]);
        return $respuesta;
    }

    public function getTareasDocentes(Request $request){
        $tareas = DB::SELECT("CALL getTareas (?);",[$request->idcurso]);
        if(!empty($tareas)){
            foreach ($tareas as $key => $post) {
                $respuesta = DB::SELECT("SELECT * FROM usuario_tarea join usuario on usuario.idusuario = usuario_tarea.usuario_idusuario WHERE tarea_idtarea = ? ",[$post->idtarea]);
                $total = DB::SELECT("SELECT count(*) as cantidad FROM usuario_tarea WHERE tarea_idtarea = ? ",[$post->idtarea]);
                $data['items'][$key] = [
                    'tarea' => $post,
                    'total'=>$total,
                    'respuesta'=>$respuesta,
                ];
            }
            return $data;
        }else{
            $data = [];
            return $data;
        }
    }

    public function quitarTareaEntregada(Request $request){
        DB::DELETE("DELETE FROM usuario_tarea WHERE id=$request->id AND tarea_idtarea = $request->idtarea;");
        
        $respuesta = DB::SELECT("SELECT * FROM usuario_tarea join usuario on usuario_idusuario = idusuario  WHERE tarea_idtarea = ? ",[$request->idtarea]);

        return $respuesta;
    }

    public function eliminarTarea(Request $request){
        DB::UPDATE("UPDATE `tarea` SET estado='0' WHERE `idtarea` = ?",[$request->id]);
    }

    public function eliminarAlumno(Request $request){
        $respuesta = DB::UPDATE("UPDATE `estudiante` SET `estado`='0' WHERE  usuario_idusuario= ? AND codigo = ?",[$request->id,$request->codigo]);
        return $respuesta;
    }

    public function postLibroCurso(Request $request){
        DB::INSERT("INSERT INTO libro_has_curso(libro_idlibro, curso_idcurso) VALUES (?,?)",[$request->idlibro,$request->idcurso]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function edit(Curso $curso)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::update("UPDATE curso SET nombre=?,seccion=?,materia=?,aula=? WHERE idcurso=?",[$request->nombre,$request->seccion,$request->materia,$request->aula,$request->idcurso]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function eliminarCurso(Request $request)
    {
        $idcurso = $request->idcurso;
        DB::UPDATE("UPDATE curso SET estado = '0' WHERE idcurso = $idcurso");
    }


    public function verif_asignatura_por_curso($id)
    {
        $cursos = DB::SELECT("SELECT *, a.nombreasignatura as label FROM curso c LEFT JOIN asignatura a ON c.id_asignatura = a.idasignatura WHERE c.idusuario = $id AND c.id_asignatura = 0 AND c.estado = '1'");
        return $cursos;
    }


    public function cargar_asignatura_curso(Request $request)
    {
        $curso = Curso::find($request->id_curso);

        $curso->id_asignatura = $request->id_asignatura;
        
        $curso->save();

        return $curso;
    }

    // CONSULTAS EXTRAS PARA LA SECCION DEL ADMINISTRADOR - inicio 05 de marzo
    public function buscarCursoCodigo($codigo)
    {
        $curso = DB::SELECT("SELECT  c.*, 
        u.cedula as cedulaDocente, u.nombres as nombreDocente, u.apellidos as apellidoDocente, u.name_usuario, u.email, u.date_created as creacionUsuario, 
        i.nombreInstitucion, g.deskripsi as grupoUsuario, e.nombreestado as estadoInstitucion
        FROM curso c, usuario u, institucion i, sys_group_users g, estado e
        WHERE c.idusuario = u.idusuario 
        and u.institucion_idInstitucion = i.idInstitucion
        and i.estado_idEstado =  e.idEstado 
        and u.id_group = g.id
        and c.codigo like '$codigo%'");
        return $curso;
    }
    // -- a.idasignatura, a.nombreasignatura, a.tipo_asignatura, a.updated_atA,  
    // --and c.id_asignatura = a.idasignatura 
    public function cursos_x_usuario($usuario)
    {
        $curso = DB::SELECT("SELECT u.cedula, u.idusuario, u.nombres, u.apellidos, u.name_usuario, u.email, u.date_created, u.estado_idEstado, u.id_group, u.institucion_idInstitucion, c.*, i.idInstitucion, i.nombreInstitucion
        FROM  usuario u, curso c, institucion i
        Where u.idusuario = c.idusuario and u.institucion_idInstitucion = i.idInstitucion and u.name_usuario like '$usuario%'");
        return $curso;
    }
    public function restaurarCurso(Request $request){

        $curso = curso::find($request->id);
        $curso->idcurso = $request->id;
        $curso->estado = $request->estado;
        $curso->save();

        return $curso;
    }
    public function cursos_x_estudiante($email)
    {
        $curso = DB::SELECT("SELECT u.cedula,  u.nombres, u.apellidos, u.name_usuario, u.email, u.date_created, u.estado_idEstado, u.id_group, i.nombreInstitucion, e.id as idestudiante, e.usuario_idusuario, e.codigo, e.updated_at, c.*, g.deskripsi as grupoNombre
        FROM  estudiante e, usuario u, institucion i, curso c, sys_group_users g
        Where u.id_group = g.id and u.idusuario = e.usuario_idusuario and e.codigo = c.codigo and u.institucion_idInstitucion = i.idInstitucion and u.email LIKE '$email%'");
        return $curso;
    }
    ///cursos por docente por asignatura seleccionada, para la seccion de PROYECTOS INTEGRADOS
    public function cursos_asignatura_docente(Request $request)
    {
        $curso = DB::SELECT(" SELECT DISTINCT c.* , a.* 
        FROM curso c, institucion_has_usuario iu, periodoescolar_has_institucion pi, periodoescolar p, asignatura a 
        WHERE c.idusuario = $request->idusuario 
        AND a.idasignatura = $request->idasignatura        
        AND c.id_asignatura = a.idasignatura
        AND c.idusuario = iu.usuario_idusuario 
        AND iu.institucion_idInstitucion = pi.institucion_idInstitucion 
        AND pi.periodoescolar_idperiodoescolar = p.idperiodoescolar 
        AND p.estado = 1 ");
        return $curso;
    }
}
