<?php

namespace App\Http\Controllers;

use App\Docente;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class DocenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $usuarios = User::where('id_group',$request->grupo)->where('estado_idEstado',1)->get(['idusuario', 'cedula', 'nombres', 'apellidos', 'name_usuario', 'email', 'id_group', 'institucion_idInstitucion', 'estado_idEstado']);
        return $usuarios;
    }

    public function tareas(Request $request){
        $tareas = DB::SELECT("SELECT tarea.* FROM curso JOIN tarea ON tarea.curso_idcurso = curso.idcurso WHERE curso.idusuario = ? AND curso.estado = '1' AND tarea.estado = '1'",[$request->idusuario]);
        return $tareas;
    }

    public function contenidos(Request $request){
        $contenido = DB::SELECT("SELECT contenido.* FROM curso JOIN contenido ON contenido.curso_idcurso = curso.idcurso WHERE curso.idusuario = ? AND curso.estado = '1' AND contenido.estado = '1'",[$request->idusuario]);
        return $contenido;
    }
    public function cant_contenido($user){
        $archivos = DB::SELECT("SELECT ct.idcontenido from contenido ct, curso cu where ct.curso_idcurso = cu.idcurso and cu.idusuario = $user");
        return $archivos;
    }
    public function cant_evaluaciones($id){
        $eval = DB::SELECT("SELECT id_docente FROM evaluaciones WHERE estado = '1' and id_docente = $id");
        return $eval;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Docente  $docente
     * @return \Illuminate\Http\Response
     */
    public function show(Docente $docente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Docente  $docente
     * @return \Illuminate\Http\Response
     */
    public function edit(Docente $docente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Docente  $docente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Docente $docente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Docente  $docente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Docente $docente)
    {
        //
    }
}
