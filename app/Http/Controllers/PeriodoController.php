<?php

namespace App\Http\Controllers;
use DB;
use App\Quotation;
use App\Periodo;
use Illuminate\Http\Request;

class PeriodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $periodo = DB::SELECT("SELECT * FROM periodoescolar");
        return $periodo;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $periodo = new Periodo();
        $periodo->fecha_inicial = $request->fecha_inicial;
        $periodo->fecha_final = $request->fecha_final;
        $periodo->region_idregion = $request->region_idregion;
        $periodo->descripcion = $request->descripcion;
        $periodo->periodoescolar = $request->descripcion;
        $periodo->save();
    }

    public function select()
    {
        $periodo = DB::select("SELECT * FROM periodoescolar inner join region on periodoescolar.region_idregion = region.idregion ");
        return $periodo;
    }
    public function institucion(Request $request)
    {
        $id=$request->idInstitucion;
        if($id == 66){
            return 1;
        }else{
            $periodo = DB::select("SELECT * 
            FROM periodoescolar_has_institucion
            INNER JOIN periodoescolar ON periodoescolar.idperiodoescolar = periodoescolar_has_institucion.periodoescolar_idperiodoescolar
            WHERE institucion_idInstitucion = ? AND periodoescolar.estado = '1' ",[$id]);
            if(empty($periodo)){
                return 0;
            }else{
                return 1;
            }
        }
    }

    public function activar(Request $request){
        $idperiodoescolar=$request->idperiodoescolar;
        $estado=$request->estado;
        echo $consulta=DB::update("UPDATE `periodoescolar` SET `estado`= ? WHERE `idperiodoescolar`= ?",[$estado,$idperiodoescolar]);        
        return $consulta;
    }

    public function desactivar(Request $request){
        $idperiodoescolar=$request->idperiodoescolar;
        $estado=$request->estado;
        echo $consulta=DB::update("UPDATE `periodoescolar` SET `estado`= ? WHERE `idperiodoescolar`= ?",[$estado,$idperiodoescolar]);
        return $consulta;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Periodo  $periodo
     * @return \Illuminate\Http\Response
     */
    public function show(Periodo $periodo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Periodo  $periodo
     * @return \Illuminate\Http\Response
     */
    public function edit(Periodo $periodo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Periodo  $periodo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Periodo $periodo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Periodo  $periodo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Periodo $periodo)
    {
        //
    }

    public function periodoActivo()
    {
        $periodo = DB::SELECT("SELECT * FROM periodoescolar WHERE estado = '1' ");
        return $periodo;
    }
}
