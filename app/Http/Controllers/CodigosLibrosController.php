<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\CodigosLibros;
use DataTables;
Use Exception;


class CodigosLibrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $auxlibros = [];
        $auxlibrosf = [];
        $auxlibros = $codigos_libros = DB::SELECT("SELECT libro.*,asignatura.* from codigoslibros join libro on libro.idlibro = codigoslibros.libro_idlibro join asignatura on asignatura.idasignatura = libro.asignatura_idasignatura  WHERE idusuario = ?",[$request->idusuario]);
        $usuario = DB::SELECT("SELECT * FROM usuario WHERE idusuario = ?",[$request->idusuario]);
        $idinstitucion = '';
        foreach ($usuario as $key => $value) {
            $idinstitucion = $value->institucion_idInstitucion;
        }
        
        if(!empty($codigos_libros)){
            foreach ($codigos_libros as $key => $value) {
                $free = DB::SELECT("SELECT libro.*,asignatura.* FROM institucion_libro join libro on libro.idlibro = institucion_libro.idlibro join asignatura on asignatura.idasignatura = libro.asignatura_idasignatura  WHERE institucion_libro.idinstitucion = ? AND asignatura.nivel_idnivel = ? AND institucion_libro.estado = '1'",[$idinstitucion,$value->nivel_idnivel]);
                foreach ($free as $keyl => $valuel) {
                    array_push($auxlibros, $valuel);
                }
            }
        }
        $auxlibrosf = array_unique($auxlibros, SORT_REGULAR);
        return $auxlibrosf;
    }
    
    public function codigosCuaderno(Request $request){
        $codigos_libros = DB::SELECT("SELECT cuaderno.* from codigoslibros join libro on libro.idlibro = codigoslibros.libro_idlibro join cuaderno on cuaderno.asignatura_idasignatura = libro.asignatura_idasignatura  WHERE idusuario = ? AND codigoslibros.codigo LIKE '%PLUS%'",[$request->idusuario]);
        return $codigos_libros;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    //CODIGO COMENTADO, SOLICITADO POR FERNANDO, 
    // public function codigos_libros_estudiante($id){
    //     $libros = DB::SELECT("SELECT l. *, c.codigo FROM libro l, codigoslibros c WHERE l.idlibro = c.libro_idlibro AND c.idusuario = $id");
    //     return $libros;
    // }
    
    public function codigos_libros_estudiante($id){
        $auxlibros = [];
        $auxlibrosf = [];
        $nivel = 0;
        $auxplanlector = [];
        $auxlibros = $codigos_libros = DB::SELECT("SELECT libro.*,asignatura.* from codigoslibros join libro on libro.idlibro = codigoslibros.libro_idlibro join asignatura on asignatura.idasignatura = libro.asignatura_idasignatura  WHERE idusuario = ?",[$id]);
        $usuario = DB::SELECT("SELECT * FROM usuario WHERE idusuario = ?",[$id]);
        $idinstitucion = '';
        foreach ($usuario as $key => $value) {
            $idinstitucion = $value->institucion_idInstitucion;
        }
        if(!empty($codigos_libros)){
            foreach ($codigos_libros as $key => $value) {
                $nivel = $value->nivel_idnivel;
                $free = DB::SELECT("SELECT libro.*,asignatura.* FROM institucion_libro join libro on libro.idlibro = institucion_libro.idlibro join asignatura on asignatura.idasignatura = libro.asignatura_idasignatura  WHERE institucion_libro.idinstitucion = ? AND asignatura.nivel_idnivel = ? AND institucion_libro.estado = '1'",[$idinstitucion,$value->nivel_idnivel]);
                foreach ($free as $keyl => $valuel) {
                    array_push($auxlibros, $valuel);
                }
            }
            $auxplanlector = DB::SELECT("CALL `freePlanlector`(?, ?);",[$nivel,$idinstitucion]);
        }
        $data=[
            'libros'=>$auxlibrosf = array_unique($auxlibros, SORT_REGULAR),
            'planlector'=>$auxplanlector,
            'nivel'=>$nivel,
            'institucion'=>$idinstitucion,
        ];
        return $data;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validacion = DB::SELECT("SELECT * FROM  codigoslibros WHERE   codigo = ?",["$request->codigo"]);
        $iduser = '';
        foreach ($validacion as $key => $value) {
            $iduser = $value->idusuario;
        }
        if(empty($validacion)){
            $data = [
                'status' => '2'
            ];
            return $data;
        }else{
            if(empty($iduser) || $iduser == 0 || $iduser == NULL ){
                DB::INSERT("INSERT INTO hist_codlibros(id_usuario, codigo_libro, idInstitucion, usuario_editor, observacion) VALUES ($request->idusuario, '$request->codigo', $request->idusuario, $request->id_institucion, 'registrado')");

                DB::UPDATE("UPDATE `codigoslibros` SET `idusuario`=?, `id_periodo`=?  WHERE `codigo`= ?",[$request->idusuario, $request->id_periodo,"$request->codigo"]);                
                $data = [
                    'status' => '1'
                ];
                return $data;
            }else{
                $data = [
                    'status' => '0'
                ];
                return $data;
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $codigos_libros = DB::SELECT("SELECT * from codigoslibros WHERE libro = '$id'");
        return $codigos_libros;
    }


    
    public function codigosLibrosFecha($datos)
    {   
        $data = explode("*", $datos);

        if( $data[0] != "" ){
            $libro = $data[0];
            $fecha = $data[1];
                
            $codigos_libros = DB::SELECT("SELECT * from codigoslibros WHERE libro = '$libro' AND created_at like '$fecha%' ORDER BY `codigoslibros`.`fecha_create` ASC");
            
            return $codigos_libros;
            
        }else{
            return 0;
        }
        
    }


    public function librosBuscar(){
        $codigos_libros = DB::SELECT("SELECT id_libro_serie as id, nombre as label from libros_series");
        return $codigos_libros;
    }




    public function codigosLibrosExportados($data){
        $datos = explode("*", $data);
        $usuario = $datos[0];
        $cantidad = $datos[1];

        $codigos_libros = DB::SELECT("SELECT * from codigoslibros WHERE idusuario = '$usuario' ORDER BY fecha_create DESC LIMIT $cantidad");

        return $codigos_libros;
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
