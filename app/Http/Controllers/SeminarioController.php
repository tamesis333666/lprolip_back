<?php

namespace App\Http\Controllers;

use App\Seminario;
use Illuminate\Http\Request;
use DB;

class SeminarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seminario = DB::SELECT("SELECT * FROM seminario WHERE estado = '1' order by fecha_inicio desc;");
        return $seminario;
    }

    public function buscarSeminario(Request $request){
        $curso = DB::SELECT("SELECT * FROM seminario WHERE idcurso = ?",[$request->idcurso]);
        $registrados = DB::SELECT("SELECT COUNT(*) as registrados FROM inscripcion join seminario on seminario.idseminario = inscripcion.seminario_idseminario WHERE seminario.idcurso = ?",[$request->idcurso]);
        $data = [
            'curso' => $curso,
            'total' => $registrados,
        ];
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }
    public function eliminarSeminario(Request $request)
    {
        DB::UPDATE("UPDATE `seminario`
        SET
        `estado` = '0'
        WHERE `idseminario` = ? ;",[$request->idcurso]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->idseminario){
            $seminario = new Seminario();
            $id = uniqid();
            $seminario->nombre = $request->nombre;
            $seminario->descripcion = $request->descripcion;
            $seminario->fecha_inicio = $request->fecha_inicio;
            $seminario->hora_inicio = $request->hora_inicio;
            $seminario->link_presentacion = $request->link_presentacion;
            $seminario->cantidad_participantes = (int) $request->cantidad_particiantes;
            $seminario->link_registro = "https://prolipadigital.com.ec/inscripciones/public/?curso=".$id;
            $seminario->idcurso = $id;
            $seminario->tiempo_curso = $request->tiempo_curso;
            $seminario->save();
        }else{
            $seminario = Seminario::find($request->idseminario);
            $seminario->nombre = $request->nombre;
            $seminario->descripcion = $request->descripcion;
            $seminario->fecha_inicio = $request->fecha_inicio;
            $seminario->hora_inicio = $request->hora_inicio;
            $seminario->link_presentacion = $request->link_presentacion;
            $seminario->cantidad_participantes = (int) $request->cantidad_particiantes;
            $seminario->tiempo_curso = $request->tiempo_curso;
            $seminario->save();
        }


        return $seminario;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\seminario  $seminario
     * @return \Illuminate\Http\Response
     */
    public function show($seminario)
    {
        $seminario = DB::SELECT("SELECT ec.cedula, s.* 
        FROM encuestas_certificados ec, seminario s 
        WHERE s.estado = '1'
        and ec.id_seminario = s.idseminario
        and ec.cedula = $seminario
        order by fecha_inicio desc;");
        return $seminario;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\seminario  $seminario
     * @return \Illuminate\Http\Response
     */
    public function edit(seminario $seminario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\seminario  $seminario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, seminario $seminario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\seminario  $seminario
     * @return \Illuminate\Http\Response
     */
    public function destroy(seminario $seminario)
    {
        return $seminario;
    }

    public function encuesta_certificados($id){
        $encuesta = DB::SELECT("SELECT ec.*, u.nombres, u.apellidos, u.email, u.telefono, i.nombreInstitucion, g.deskripsi as grupo
        FROM encuestas_certificados ec, usuario u, institucion i, sys_group_users g 
        WHERE id_seminario = $id
        and ec.cedula = u.cedula
        and u.id_group = g.id
        and u.institucion_idInstitucion = i.idInstitucion");
        return $encuesta;
    }

    public function seminariosDocente($id)
    {
        $seminarios = DB::SELECT("SELECT s.*, i.* FROM seminario s, inscripcion i WHERE s.idseminario = i.seminario_idseminario AND i.cedula LIKE '$id' ORDER BY s.fecha_inicio DESC");
        return $seminarios;
    }
}
