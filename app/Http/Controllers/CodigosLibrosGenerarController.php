<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\CodigosLibros;
use DataTables;
Use Exception;


class CodigosLibrosGenerarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $codigos_libros = DB::SELECT("SELECT * from codigoslibros limit 100");
        return $codigos_libros;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        set_time_limit(6000);
        ini_set('max_execution_time', 6000);
		
        $porcentaje = 0;

        $codigos = explode(",", $request->codigo);
        $tam = sizeof($codigos);

        for( $i=0; $i<$tam; $i++ ){
            $codigos_libros = new CodigosLibros();

            $codigos_libros->serie = $request->serie;
            $codigos_libros->libro = $request->libro;
            $codigos_libros->anio = $request->anio;
            $codigos_libros->libro_idlibro = $request->idlibro;
            $codigos_libros->estado = $request->estado;
			$codigos_libros->idusuario = 0;
            $codigos_libros->idusuario_creador_codigo = $request->idusuario;

            $codigo_verificar = $codigos[$i];
            $verificar_codigo = DB::SELECT("SELECT codigo from codigoslibros WHERE codigo = '$codigo_verificar'");

            if( $verificar_codigo ){
                
            }else{
                $codigos_libros->codigo = $codigos[$i];
                $codigos_libros->save();
                $porcentaje++;
            }

        }

        return $porcentaje;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function editarCodigoBuscado($datos)
    {
        
        $data = explode("*", $datos);
        $codigo = $data[0];
        $libro = $data[1];
        $serie = $data[2];
        $anio = $data[3];
        $idusuario_creador_codigo = $data[4];
        $idusuario = $data[5];
        $idLibro = $data[6];
        $id_periodo = $data[7];
        $id_institucion = $data[8];

        if( $codigo != "" ){

            $has_periodo = DB::SELECT("SELECT `id_periodo` FROM `codigoslibros` WHERE `codigo` = '$codigo'");
            if( $has_periodo[0]->id_periodo == null ){
                $codigos_libros = DB::UPDATE("UPDATE `codigoslibros` SET `codigo`='$codigo',`serie`='$serie',`libro`='$libro',`anio`='$anio',`idusuario_creador_codigo`=$idusuario_creador_codigo,`idusuario`='$idusuario', `libro_idlibro`=$idLibro,`id_periodo` = $id_periodo WHERE `codigo`= '$codigo'");
            }else{
                $codigos_libros = DB::UPDATE("UPDATE `codigoslibros` SET `codigo`='$codigo',`serie`='$serie',`libro`='$libro',`anio`='$anio',`idusuario_creador_codigo`=$idusuario_creador_codigo,`idusuario`='$idusuario', `libro_idlibro`=$idLibro WHERE `codigo`= '$codigo'");
            }
            
            
            DB::INSERT("INSERT INTO hist_codlibros(id_usuario, codigo_libro, idInstitucion, usuario_editor, observacion) VALUES ($idusuario, '$codigo', $id_institucion, $idusuario_creador_codigo, 'modificado')");

            return $codigos_libros;

        }else{

            return 'Codigo no encontrado';

        }

    }


    public function show($id)
    {
        $codigos_libros = DB::SELECT("SELECT * from codigoslibros WHERE libro = '$id'");
        return $codigos_libros;
    }


    
    public function codigosLibrosFecha($datos)
    {   
        $data = explode("*", $datos);

        if( $data[0] != "" ){
            $datalibro = explode("-", $data[0]);
            $fecha = $data[1];

            $libro = $datalibro[1];
            $serie = $datalibro[0];
            //SELECT c.idusuario, c.codigo, l.nombrelibro as libro, c.serie, c.anio, c.fecha_create, s.id_serie, s.nombre_serie, c.libro_idlibro, u.nombres, u.apellidos, u.cedula, i.nombreInstitucion from codigoslibros c, series s, libro l, usuario u, institucion i WHERE c.serie = s.nombre_serie AND c.libro_idlibro = l.idlibro AND c.libro = '$libro' AND c.serie = '$serie' AND c.created_at like '$fecha%' AND c.idusuario = u.idusuario AND u.institucion_idInstitucion = i.idInstitucion
            $codigos_libros = DB::SELECT("SELECT c.idusuario, c.codigo, l.nombrelibro as libro, c.serie, c.anio, c.fecha_create, s.id_serie, s.nombre_serie, c.libro_idlibro, u.nombres, u.apellidos, u.cedula, i.nombreInstitucion FROM codigoslibros c INNER JOIN series s ON c.serie = s.nombre_serie INNER JOIN libro l ON c.libro_idlibro = l.idlibro LEFT JOIN usuario u ON c.idusuario = u.idusuario LEFT JOIN institucion i ON u.institucion_idInstitucion = i.idInstitucion WHERE c.libro = '$libro' AND c.serie = '$serie' AND c.created_at like '$fecha%'");
            
            return $codigos_libros;
            
        }else{
            return 0;
        }
        
    }

    
    public function codigosLibrosCodigo($codigo)
    {
        $codigos_libros = DB::SELECT("SELECT c.codigo, c.serie, l.nombrelibro as libro, c.anio, c.idusuario, c.idusuario_creador_codigo, c.libro_idlibro, c.estado, c.fecha_create, c.created_at, c.updated_at, s.id_serie, s.nombre_serie, s.longitud_numeros, s.longitud_letras, u.nombres, u.apellidos, u.cedula, i.nombreInstitucion FROM codigoslibros c INNER JOIN series s ON c.serie = s.nombre_serie INNER JOIN libro l ON c.libro_idlibro = l.idlibro LEFT JOIN usuario u ON c.idusuario = u.idusuario LEFT JOIN institucion i ON u.institucion_idInstitucion = i.idInstitucion WHERE c.codigo like '%$codigo%'");
            
        return $codigos_libros;
    }


    public function librosBuscar(){//select buscar
        //SELECT l.id_libro_serie as id, concat_ws('-', s.nombre_serie, l.nombre) as label from libros_series l, series s WHERE l.id_serie = s.id_serie
        $codigos_libros = DB::SELECT("SELECT l.id_libro_serie as id, concat_ws('-', s.nombre_serie, l.nombre) as label from libros_series l, series s WHERE l.id_serie = s.id_serie");
        return $codigos_libros;
    }




    public function codigosLibrosExportados($data){
        $datos = explode("*", $data);
        $usuario = $datos[0];
        $cantidad = $datos[1];

        $codigos_libros = DB::SELECT("SELECT * from codigoslibros WHERE idusuario_creador_codigo = '$usuario' ORDER BY fecha_create DESC LIMIT $cantidad");

        return $codigos_libros;
    }

    

    public function reportesCodigoInst(Request $request){

        $codigos_libros = DB::SELECT("(SELECT COUNT(c.codigo) as cantidad, GROUP_CONCAT(DISTINCT c.serie) as serie, (SELECT l.nombrelibro FROM libro l WHERE l.idlibro = (GROUP_CONCAT(DISTINCT c.libro_idlibro)) ) as libro, GROUP_CONCAT(DISTINCT (SELECT ciudad.nombre FROM ciudad WHERE ciudad.idciudad = i.ciudad_id) ) as ciudad, GROUP_CONCAT(DISTINCT (SELECT GROUP_CONCAT(usuario.nombres,' ',usuario.apellidos) as vendedor FROM usuario WHERE usuario.cedula = (SELECT institucion.vendedorInstitucion FROM institucion WHERE institucion.idInstitucion = i.idInstitucion ) ) ) as asesor, GROUP_CONCAT(DISTINCT (SELECT i.nombreInstitucion FROM institucion WHERE institucion.idInstitucion = i.idInstitucion ) ) as institucion FROM codigoslibros c, usuario u, institucion i WHERE c.idusuario = u.idusuario AND u.institucion_idInstitucion = i.idInstitucion AND i.idInstitucion = $request->id AND c.updated_at BETWEEN CAST('$request->fromdate' AS DATE) AND CAST('$request->todate' AS DATE) AND c.codigo not like '%plus%' GROUP BY c.libro_idlibro ORDER BY `c`.`updated_at`  DESC) UNION (SELECT COUNT(c.codigo) as cantidad, GROUP_CONCAT(DISTINCT c.serie, ' PLUS') as serie, (SELECT l.nombrelibro FROM libro l WHERE l.idlibro = (GROUP_CONCAT(DISTINCT c.libro_idlibro)) ) as libro, GROUP_CONCAT(DISTINCT (SELECT ciudad.nombre FROM ciudad WHERE ciudad.idciudad = i.ciudad_id) ) as ciudad, GROUP_CONCAT(DISTINCT (SELECT GROUP_CONCAT(usuario.nombres,' ',usuario.apellidos) as vendedor FROM usuario WHERE usuario.cedula = (SELECT institucion.vendedorInstitucion FROM institucion WHERE institucion.idInstitucion = i.idInstitucion ) ) ) as asesor, GROUP_CONCAT(DISTINCT (SELECT i.nombreInstitucion FROM institucion WHERE institucion.idInstitucion = i.idInstitucion ) ) as institucion FROM codigoslibros c, usuario u, institucion i WHERE c.idusuario = u.idusuario AND u.institucion_idInstitucion = i.idInstitucion AND i.idInstitucion = $request->id AND c.updated_at BETWEEN CAST('$request->fromdate' AS DATE) AND CAST('$request->todate' AS DATE) AND c.codigo like '%plus%' GROUP BY c.libro_idlibro ORDER BY `c`.`updated_at`  DESC)");

        return $codigos_libros;
    }


    public function reportesCodigoAsesor($id){
        $codigos_libros = DB::SELECT("SELECT codigoslibros.codigo, (SELECT l.nombrelibro FROM libro l WHERE l.idlibro = codigoslibros.libro_idlibro) as libro, codigoslibros.updated_at as registrado, usuario.cedula as cedula, usuario.nombres, usuario.apellidos, usuario.email from codigoslibros JOIN usuario on usuario.idusuario = codigoslibros.idusuario WHERE usuario.institucion_idInstitucion = $id");
        return $codigos_libros;
    }

    
    public function seriesCambiar(){
        $codigos_libros = DB::SELECT("SELECT id_serie as id, nombre_serie as label from series");

        return $codigos_libros;
    }

    
    public function librosSerieCambiar($id){
        $codigos_libros = DB::SELECT("SELECT idLibro as id, nombre as label from libros_series WHERE id_serie = $id");

        return $codigos_libros;
    }

    
    public function institucionesResportes(){
        $instituciones = DB::SELECT("SELECT i.idInstitucion as id, i.nombreInstitucion as label, c.nombre as nombre_ciudad, pi.periodoescolar_idperiodoescolar as id_periodo from institucion i, ciudad c, periodoescolar_has_institucion pi WHERE i.ciudad_id = c.idciudad AND i.idInstitucion = pi.institucion_idInstitucion AND pi.id = (SELECT MAX(phi.id) AS periodo_maximo FROM periodoescolar_has_institucion phi WHERE phi.institucion_idInstitucion = i.idInstitucion)");

        return $instituciones;
    }


    public function editarInstEstud(Request $request){
        $institucion = DB::UPDATE("UPDATE usuario SET institucion_idInstitucion=$request->idInstitucion WHERE idusuario=$request->id;");

        return $institucion;
    }



    public function librosCambiar($id){
        $codigos_libros = DB::SELECT("SELECT * from libros_series WHERE idLibro = $id");

        return $codigos_libros;
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function cambioEstadoCodigo(Request $request)
    {        
        $cambio = CodigosLibros::find($request->codigo);
        $cambio->estado = $request->estado;
        
        $cambio->save();
        
        return $cambio;
        // return $request;
    }
    //agregar codigos perdidos, solicitados por soporte
    public function agregar_codigo_perdido(Request $request)
    {
        $agregar = new CodigosLibros();
        $agregar->codigo = $request->codigo;
        $agregar->serie = $request->serie;
        $agregar->libro = $request->libro;
        $agregar->anio = $request->anio;
        $agregar->idusuario =$request->idusuario;
        $agregar->idusuario_creador_codigo = $request->idusuario_creador_codigo;
        $agregar->libro_idlibro = $request->libro_idlibro;
        $agregar->estado = $request->estado;        
        $agregar->save();
        return $agregar;   
    }
//busqueda de codigos de libros registrados y eliminados por los estudiantes
    public function getHistoricoCodigos($id)
    {
        $codigos = DB::SELECT("SELECT his.codigo_libro, his.observacion, his.created_at as fecha_observacion, u.cedula, u.nombres, u.apellidos, u.email, u.estado_idEstado, i.nombreInstitucion, c.nombre as ciudad, cd.serie, cd.libro, cd.anio, cd.estado, cd.fecha_create
        from hist_codlibros his, usuario u, institucion i, ciudad c, codigoslibros cd
        WHERE his.id_usuario = u.idusuario
        AND u.institucion_idInstitucion = i.idInstitucion
        AND i.ciudad_id = c.idciudad
        AND his.codigo_libro = cd.codigo
        AND u.cedula  = '$id'");
        
        return $codigos;
    }

    // metodo para cargar el id del periodo actual del estudiante al cual se le haya asignado cada codigo
    public function cargarPeriodoCodigo()
    {
        set_time_limit(60000);
        ini_set('max_execution_time', 60000);

        $codigos = DB::SELECT("SELECT c.codigo, p.idperiodoescolar FROM codigoslibros c, usuario u, periodoescolar_has_institucion pi, periodoescolar p WHERE c.idusuario IS NOT null AND c.idusuario != 0 AND c.idusuario = u.idusuario AND u.institucion_idInstitucion = pi.institucion_idInstitucion AND pi.periodoescolar_idperiodoescolar = p.idperiodoescolar AND pi.id = (SELECT MAX(phi.id) AS periodo_maximo FROM periodoescolar_has_institucion phi WHERE phi.institucion_idInstitucion = pi.institucion_idInstitucion) AND c.id_periodo IS null");
        
        foreach ($codigos as $key => $value) {
            DB::UPDATE("UPDATE `codigoslibros` SET `id_periodo` = ? WHERE `codigo` = ?", [$value->idperiodoescolar, $value->codigo]);
        }
    }

}
