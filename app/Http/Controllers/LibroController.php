<?php

namespace App\Http\Controllers;

use App\Libro;
use Illuminate\Http\Request;
use DB;
use App\Quotation;
use DateTime;
class LibroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $buscar = $request->buscar;
        $criterio = $request->criterio;
        $idusuario = $request->idusuario;
        $idInstitucion = $request->idinstitucion;
        if($idInstitucion == 66){
            $libro = DB::select("SELECT libro.*,asignatura.* FROM libro join asignatura on asignatura.idasignatura = libro.asignatura_idasignatura  ORDER BY  `libro`.`asignatura_idasignatura` ASC ");
        }else{
            $libro = DB::select('CALL datoslibrosd(?)',[$idusuario]);
        }
        return $libro;
    }
    public function librosEstudiante(Request $request)
    {
        $idregion='';
        $buscar = $request->buscar;
        $criterio = $request->criterio;
        $idusuario = $request->idusuario;
        $region = DB::SELECT("SELECT * FROM `usuario` JOIN institucion ON institucion.idInstitucion = usuario.institucion_idInstitucion WHERE `idusuario`= ?",[$idusuario]);
        foreach ($region as $key) {
            $idregion = $key->region_idregion;
        }
        if($idregion == 1){
            
            $libro = DB::select('CALL datoslibrosEstudianteSierra(?)',[$idusuario]);
            return $libro;

        }else{
            
            $libro = DB::select('CALL datoslibrosEstudiante(?)',[$idusuario]);
            return $libro;

        }
    }

    public function Historial(Request $request){
        $date = new DateTime();
        $idusuario = auth()->user()->idusuario;
        $idlibro = $request->idlibro;
        $fecha = $date->format('y-m-d');
        $hora = $date->format('H:i:s');
        DB::insert("INSERT INTO `libro_has_usuario`(`libro_idlibro`, `usuario_idusuario`, `fecha`, `hora`) VALUES (?,?,?,?)",[$idlibro,$idusuario,$fecha,$hora]);
    }

    public function aplicativo(Request $request)
    {
        $libros = DB::select('CALL datoslibrosd(?)',[$request->idusuario]);
        return $libros;
    }

    public function aplicativoEstudiante(Request $request)
    {
        $libros = DB::select("SELECT libro.* FROM `estudiante` JOIN curso ON curso.codigo = estudiante.codigo JOIN libro_has_curso ON libro_has_curso.curso_idcurso = curso.idcurso join libro ON libro.idlibro = libro_has_curso.libro_idlibro WHERE estudiante.usuario_idusuario = ? AND curso.estado = '1' AND libro.grupo = '1'",[$request->idusuario]);
        return $libros;
    }

    public function libro(Request $request)
    {
        if($request->idgrupo == 1){
            $libro = DB::select('SELECT * FROM libro');
            return $libro;
        }
        
        if($request->idgrupo == 11){
            switch ($request->idinstitucion) {
                case 66:
                    $libro = DB::select('SELECT libro.* FROM libros_region_free join libro on libro.idlibro=libros_region_free.libro');
                    return $libro;
                break;
                case 905:
                    $libro = DB::select('SELECT libro.* FROM libros_region_free join libro on libro.idlibro=libros_region_free.libro');
                    return $libro;
                break;
            }
        }
    }

    public function planlector(Request $request)
    {
        if($request->idgrupo == 1){
            $planlector = DB::select('SELECT * FROM planlector WHERE planlector.estado_idEstado = "1"');
            return $planlector;
        }
        
        if($request->idgrupo == 11){
            switch ($request->idinstitucion) {
                case 66:
                    $planlector = DB::select('SELECT planlector.* FROM planlector_region_free join planlector on planlector.idplanlector=planlector_region_free.planlector WHERE planlector.estado_idEstado = "1"');
                    return $planlector;
                break;
                case 905:
                    $planlector = DB::select('SELECT planlector.* FROM planlector_region_free join planlector on planlector.idplanlector=planlector_region_free.planlector WHERE planlector.estado_idEstado = "1"');
                    return $planlector;
                break;
            }
        }
    }

    public function setNivelFree(Request $request)
    {
        $niveles = explode(",", $request->niveles);
        try {
            DB::delete('DELETE FROM `planlector_nivel` WHERE `institucion_planlector` = ?', [$request->id]);
            foreach ($niveles as $key => $value) {
                DB::insert('insert into planlector_nivel (institucion_planlector, nivel) values (?, ?)', [$request->id, $value]);
            }
        } catch (\Throwable $th) {
            foreach ($niveles as $key => $value) {
                DB::insert('insert into planlector_nivel (institucion_planlector, nivel) values (?, ?)', [$request->id, $value]);
            }
        }
        // return $request->niveles;
    }

    public function libroFree(Request $request){
        $libro = DB::INSERT("INSERT INTO institucion_libro(idinstitucion, idlibro) VALUES (?,?)",[$request->idinstitucion, $request->idlibro]);
    }

    public function planlectorFree(Request $request){
        $libro = DB::INSERT("INSERT INTO institucion_planlector(idinstitucion, idplanlector) VALUES (?,?)",[$request->idinstitucion, $request->idplanlector]);
    }

    public function listaFree(Request $request){
        $libros = DB::SELECT("SELECT * FROM institucion_libro join libro on libro.idlibro = institucion_libro.idlibro join asignatura on asignatura.idasignatura = libro.asignatura_idasignatura WHERE institucion_libro.idinstitucion = ? AND institucion_libro.estado = '1'",[$request->idinstitucion]);
        foreach ($libros as $key => $post) {
            $respuesta = DB::SELECT("SELECT * FROM libro_nivel join nivel on nivel.idnivel = libro_nivel.nivel WHERE institucion_libro = ? ",[$post->id]);
            $data['items'][$key] = [
                'id' => $post->id,
                'idinstitucion' => $post->idinstitucion,
                'idlibro' => $post->idlibro,
                'nombrelibro' => $post->nombrelibro,
                'nombreasignatura' => $post->nombreasignatura,
                'estado' => $post->estado,
                'niveles'=>$respuesta,
            ];
        }
        return $data;
    }

    public function listaFreePlanlector(Request $request){
        try {
            $data['items'] = [];
            //code...
            $planlectors = DB::SELECT("SELECT * FROM institucion_planlector join planlector on planlector.idplanlector = institucion_planlector.idplanlector WHERE institucion_planlector.idinstitucion = ?
            AND institucion_planlector.estado = 1",[$request->idinstitucion]);
            foreach ($planlectors as $key => $post) {
                $respuesta = DB::SELECT("SELECT * FROM planlector_nivel join nivel on nivel.idnivel = planlector_nivel.nivel WHERE institucion_planlector = ? ",[$post->id]);
                $data['items'][$key] = [
                    'id' => $post->id,
                    'idinstitucion' => $post->idinstitucion,
                    'idplanlector' => $post->idplanlector,
                    'nombreplanlector' => $post->nombreplanlector,
                    'estado' => $post->estado,
                    'niveles'=>$respuesta,
                ];
            }
        } catch (\Throwable $th) {
            $data['items'] = [];
        }
        return $data;
    }

    public function eliminarLibroFree(Request $request){
        DB::DELETE("UPDATE `institucion_libro` SET `estado`='0' WHERE  `id` = ?",[$request->id]);
    }

    public function eliminarPlanlectorFree(Request $request){
        $resp = DB::DELETE("UPDATE `institucion_planlector` SET `estado`= 0 WHERE  `id` = ?",[$request->id]);
        return $resp;
    }
    
    public function audio(Request $request)
    {
        $buscar = $request->buscar;
        $criterio = $request->criterio;
        $idusuario = auth()->user()->idusuario;
        $libro = DB::select('CALL datoslibrosdd(?)',[$idusuario]);
        return $libro;
    }

    public function registraringreso(){
        $idusuario = auth()->user()->idusuario;
        $ip = $_SERVER['REMOTE_ADDR'];
        $navegador = "GoogleChrome";
        DB::insert("INSERT INTO `registro_usuario`( `ip`, `navegador`, `usuario_idusuario`) VALUES (?,?,?)",["$ip","$navegador",$idusuario]);
        //DB::update("UPDATE `usuario` SET `p_ingreso`=?   WHERE `idusuario` = ?",['1',$idusuario]);
    }



    
    public function quitarlibroestudiante(Request $request){

        $buscarCodigo = DB::SELECT("SELECT codigoslibros.codigo FROM `codigoslibros` WHERE idusuario = ? AND libro_idlibro=?",[$request->idusuario,$request->idlibro]);
        DB::INSERT("INSERT INTO hist_codlibros(id_usuario, codigo_libro, observacion) VALUES (?,?,?)",[$request->idusuario, $buscarCodigo[0]->codigo, 'eliminado']);
        $libro = DB::UPDATE("UPDATE `codigoslibros` SET `idusuario` = 0 WHERE `idusuario` = $request->idusuario AND `libro_idlibro` = $request->idlibro");
        //registro en el historico de codigos al quitar el libro del estudiante
        return $libro;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Libro::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function show(Libro $libro)
    {
        $libros = DB::select('CALL datoslibrosdd(?)',[$libro]);
        return $libros;
    }


    
    public function desgloselibrousuario($libro)
    {
        $libro = DB::select('CALL desgloselibro(?)',[$libro]);
        return $libro;
    }


    
    public function menu_unidades_libros($libro)
    {
        $unidades = DB::SELECT('SELECT u.*, l.weblibro FROM unidades_libros u, libro l WHERE u.id_libro = l.idlibro AND u.id_libro = ?',[$libro]);
        return $unidades;
    }
    
    
    public function unidades_asignatura($idasignatura)
    {
        $unidades = DB::SELECT('SELECT u .*, concat(u.unidad, " - ", u.nombre_unidad) as label_unidad, l.weblibro, concat(u.unidad, " - ", u.nombre_unidad) as label, u.id_unidad_libro as id FROM unidades_libros u, libro l WHERE u.id_libro = l.idlibro AND l.asignatura_idasignatura = ? ORDER BY u.unidad',[$idasignatura]);
        return $unidades;
    }

    public function planificacionesunidades_tema($id_tema)
    {
        $animaciones = DB::SELECT('SELECT * FROM actividades_animaciones aa, temas t WHERE aa.id_tema = t.id AND aa.tipo = 1 AND t.id_unidad = ?',[$id_tema]);

        return $animaciones;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function edit(Libro $libro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $respuesta=DB::update('UPDATE libro SET nombrelibro = ? ,descripcionlibro = ? ,weblibro = ? ,exelibro = ? ,pdfsinguia = ? ,pdfconguia = ? ,guiadidactica = ? ,Estado_idEstado = ? ,asignatura_idasignatura = ? ,ziplibro = ?  WHERE idlibro = ?',[$request->nombrelibro,$request->descripcionlibro,$request->weblibro,$request->exelibro,$request->pdfsinguia,$request->pdfconguia,$request->guiadidactica,$request->Estado_idEstado,$request->asignatura_idasignatura,$request->ziplibro,$request->idlibro]);
        return $respuesta;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::delete('DELETE FROM libro WHERE idlibro = ?',[$request->idlibro]);
    }
}