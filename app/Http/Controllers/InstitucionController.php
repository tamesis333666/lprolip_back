<?php

namespace App\Http\Controllers;

use App\Institucion;
use App\PeriodoInstitucion;
use Illuminate\Http\Request;
use DB;
use App\Quotation;
class InstitucionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $institucion = DB::select("CALL `listar_instituciones_periodo_activo` ();");
        return $institucion;
        
    }

    public function selectInstitucion(Request $request){
        if(empty($request->idregion) && empty($request->idciudad)){
            $institucion = DB::SELECT("SELECT idInstitucion,UPPER(nombreInstitucion) as nombreInstitucion FROM institucion WHERE idInstitucion != 66 AND estado_idEstado = 1");
        }
        if(!empty($request->idregion) && empty($request->idciudad)){
            $institucion = DB::SELECT("SELECT idInstitucion,UPPER(nombreInstitucion) as nombreInstitucion FROM institucion WHERE region_idregion = ? AND idInstitucion != 66 AND estado_idEstado = 1",[$request->idregion]);
        }
        if(!empty($request->idciudad) && empty($request->idregion)){
            $institucion = DB::SELECT("SELECT idInstitucion,UPPER(nombreInstitucion) as nombreInstitucion FROM institucion WHERE ciudad_id = ? AND idInstitucion != 66 AND estado_idEstado = 1",[$request->idciudad]);
        }
        if(!empty($request->idciudad) && !empty($request->idregion)){
            $institucion = DB::SELECT("SELECT idInstitucion,UPPER(nombreInstitucion) as nombreInstitucion FROM institucion WHERE ciudad_id = ? AND region_idregion = ? AND idInstitucion != 66 AND estado_idEstado = 1",[$request->idciudad,$request->idregion]);
        }
        return $institucion;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datosValidados=$request->validate([
            'nombreInstitucion' => 'required',
            'telefonoInstitucion' => 'required',
            'direccionInstitucion' => 'required',
            'vendedorInstitucion' => 'required',
            'region_idregion' => 'required',
            'solicitudInstitucion' => 'required',
            'ciudad_id' => 'required',
            'periodoescolar' => 'required',
        ]);

        if(!empty($request->idInstitucion)){
            $institucion = Institucion::find($request->idInstitucion)->update($request->all());
            return $institucion;
        }else{
            $institucion = new Institucion($request->all());
            $institucion->save();
            $periodoInstitucion = new PeriodoInstitucion();
            $periodoInstitucion->institucion_idInstitucion = $institucion->idInstitucion;
            $periodoInstitucion->periodoescolar_idperiodoescolar = $request->periodoescolar;
            $periodoInstitucion->save();
            return $institucion;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Institucion  $institucion
     * @return \Illuminate\Http\Response
     */
    public function show(Institucion $institucion)
    {
        return $institucion;
    }


    public function verInstitucionCiudad($idciudad)
    {   
        $instituciones = DB::SELECT("SELECT idInstitucion as id, nombreInstitucion as label FROM institucion WHERE idInstitucion != 66 AND ciudad_id = $idciudad");

        return $instituciones;
    }

    
    public function verificarInstitucion($id)
    {   
        $instituciones = DB::SELECT("SELECT institucion_idInstitucion FROM usuario WHERE idusuario = $id");

        return $instituciones;
    }

    
    public function asignarInstitucion(Request $request)
    {   
        $institucion = DB::UPDATE("UPDATE usuario SET institucion_idInstitucion = $request->institucion WHERE idusuario = $request->usuario");

        return $institucion;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Institucion  $institucion
     * @return \Illuminate\Http\Response
     */
    public function edit(Institucion $institucion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Institucion  $institucion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Institucion $institucion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Institucion  $institucion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Institucion $institucion)
    {
        $institucion = Institucion::find($institucion->idarea)->update(['estado_idEstado' => '0']);
        return $institucion;
    }


    //guardar foto de institucion desde perfil de director
    public function guardarLogoInstitucion(Request $request)
    {
        $cambio = Institucion::find($request->institucion_idInstitucion);

        $ruta = public_path('/instituciones_logos');
        if(!empty($request->file('archivo'))){
        $file = $request->file('archivo');
        $fileName = uniqid().$file->getClientOriginalName();
        $file->move($ruta,$fileName);
        $cambio->imgenInstitucion = $fileName;
        }

        $cambio->ideditor = $request->ideditor;
        $cambio->nombreInstitucion = $request->nombreInstitucion;
        $cambio->direccionInstitucion = $request->direccionInstitucion;
        $cambio->telefonoInstitucion = $request->telefonoInstitucion;
        $cambio->region_idregion = $request->region_idregion;
        $cambio->ciudad_id = $request->ciudad_id;
        $cambio->updated_at = now();
        
        $cambio->save();
        return $cambio;

    }
    public function institucionesSalle()
    {
        $institucion = DB::select("SELECT nombreInstitucion, idInstitucion FROM  institucion  WHERE tipo_institucion = 2 and estado_idEstado = 1 ");
        return $institucion;
    }
}