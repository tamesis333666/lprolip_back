<?php

namespace App\Http\Controllers;

use App\LibroSerie;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libro;
use App\Series;
use Illuminate\Support\Facades\DB;


class LibroSerieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $librosSerie = LibroSerie::orderBy('id_libro_serie','desc')->get();

          $libro = Libro::all();
          $series = Series::all();

        $librosSerie = DB::table('libros_series')
      ->join('series', 'libros_series.id_serie', '=', 'series.id_serie')
      ->join('libro','libro.idlibro','=','libros_series.idLibro')
      ->select('libro.nombrelibro','libro.idlibro','series.nombre_serie','series.id_serie','libros_series.id_libro_serie','libros_series.iniciales', 'libros_series.codigo_liquidacion','libros_series.nombre','libros_series.version','libros_series.boton','libros_series.year','libros_series.estado')
     ->orderBy('id_libro_serie','desc')
      ->get();

       return  ['librosSerie' => $librosSerie, 'libroslista' => $libro, 'serieslista' => $series];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    $libro = Libro::all();
    $series = Series::all();
    return [
        'series' => $series,
       'libro' => $libro,

    ];


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       


        if( $request->id ){
            $librosSerie = LibroSerie::find($request->id);
        }else{
            $librosSerie = new LibroSerie();
        }

        $librosSerie->idLibro = $request->idLibro;
        $librosSerie->id_serie = $request->id_serie;
        $librosSerie->iniciales = $request->iniciales;
        $librosSerie->codigo_liquidacion = $request->codigo_liquidacion;
        $librosSerie->nombre = $request->nombre;
        $librosSerie->year = $request->year;
        $librosSerie->version = $request->version2;
        $librosSerie->boton = "success";

       
        $librosSerie->save();

        return $librosSerie;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LibroSerie  $libroSerie
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LibroSerie  $libroSerie
     * @return \Illuminate\Http\Response
     */
    public function edit(LibroSerie $libroSerie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LibroSerie  $libroSerie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LibroSerie $libroSerie)
    {
        $libroSerie->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LibroSerie  $libroSerie
     * @return \Illuminate\Http\Response
     */
  public function destroy($id)
    {
        $libroSerie = LibroSerie::findOrFail($id);
        $libroSerie->delete();
        return response()->json($libroSerie);

    }



     public function desactivar(Request $request)
    {
        
        $libroSerie =  LibroSerie::findOrFail($request->get('id_libro_serie'));
        
        $libroSerie->estado = 0;
        $libroSerie->save();
        return response()->json($libroSerie);
    }

     public function activar(Request $request)
    {
       
        $libroSerie =  LibroSerie::findOrFail($request->get('id_libro_serie'));
        
        $libroSerie->estado = 1;
        $libroSerie->save();
        return response()->json($libroSerie);
    }
}
