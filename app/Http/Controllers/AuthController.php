<?php
namespace App\Http\Controllers;
use App\Http\Requests\RegisterAuthRequest;
use App\User;
use DB;
use App\Quotation;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
class AuthController extends Controller {
    public $loginAfterSignUp = true;

    public function register(Request $request) {

        $datosValidados=$request->validate([
            'cedula' => 'required|max:11|unique:usuario',
            'password' => 'required|min:8',
            'nombres' => 'required',
            'apellidos' => 'required',
            'email' => 'required|email|unique:usuario',
            'institucion' => 'required',
            'grado' => 'required|max:20',
            'paralelo' => 'required|max:20',
        ]);

        $user = new User();
        $user->cedula = $request->cedula;
        $user->nombres = $request->nombres;
        $user->apellidos = $request->apellidos;
        $user->email = $request->email;
        $user->name_usuario = $request->email;
        $user->id_group = '4';
        $user->institucion_idInstitucion = $request->institucion;
        $user->password = sha1(md5($request->password));
        $user->save();
        if ($this->loginAfterSignUp) {
        return $this->login($request);
        }
        return response()->json([
        'status' => 'ok',
        'data' => $user
        ], 200);
    }
    public function login(Request $request) {
        $usuario = DB::SELECT("SELECT u . *, pi.periodoescolar_idperiodoescolar, i.nombreInstitucion FROM usuario u LEFT JOIN periodoescolar_has_institucion pi ON u.institucion_idInstitucion = pi.institucion_idInstitucion JOIN institucion i ON pi.institucion_idInstitucion = i.idInstitucion WHERE u.name_usuario = ? AND u.password = ? AND pi.id = (SELECT MAX(phi.id) AS periodo_maximo FROM periodoescolar_has_institucion phi WHERE phi.institucion_idInstitucion = pi.institucion_idInstitucion)",[$request->name_usuario,sha1(md5($request->password))]);           
        $input = $request->only('name_usuario', 'password');
        $jwt_token = JWTAuth::attempt($input);
        if (!$jwt_token = JWTAuth::attempt($input)) {
            return response()->json([
            'status' => 'invalid_credentials',
            'message' => 'Correo o contraseña no válidos.',
            ], 401);
        }else{
            $id = 0;
            foreach ($usuario as $key => $value) {
                $id = $value->idusuario;
            }
            DB::update('update usuario set remember_token = ? where idusuario = ?', [$jwt_token,$id]);
            return response()->json([
                'datos' =>$usuario,
                'status' => 'ok',
                'token' => $jwt_token,
            ]);
        }
    }
    public function logout(Request $request) {
        $this->validate($request, [
            'token' => 'required'
        ]);
        try {
            JWTAuth::invalidate($request->token);
            return response()->json([
            'status' => 'ok',
            'message' => 'Cierre de sesión exitoso.'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'status' => 'unknown_error',
            'message' => 'Al usuario no se le pudo cerrar la sesión.'
            ], 500);
        }
    }
    public function getAuthUser(Request $request) {
        $this->validate($request, [
            'token' => 'required'
        ]);
        $user = JWTAuth::authenticate($request->token);
        return response()->json(['user' => $user]);
    }
    protected function jsonResponse($data, $code = 200){
        return response()->json($data, $code,
        ['Content-Type' => 'application/json;charset=UTF8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
    }
}

