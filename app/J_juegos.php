<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class J_juegos extends Model
{
    protected $table = "j_juegos";
    protected $primaryKey = 'id_juego';
}
