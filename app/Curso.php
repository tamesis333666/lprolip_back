<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $table = "curso";
    protected $primaryKey = 'idcurso';
    protected $fillable = [
        'nombre',
        'seccion',
        'materia',
        'aula',
        'codigo',
        'idusuario',
        'id_asignatura',
    ];
	public $timestamps = false;
}
