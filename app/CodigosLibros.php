<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodigosLibros extends Model
{
    protected $table = "codigoslibros";
    protected $primaryKey = 'codigo';
}
