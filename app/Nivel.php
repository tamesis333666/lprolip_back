<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nivel extends Model
{
    protected $table = "nivel";
    protected $primaryKey = 'idnivel';
    protected $fillable = [
        'nombrenivel',
    ];
	public $timestamps = false;
}
