<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class estudiante extends Model
{
    protected $table = "estudiante";
    protected $fillable = [
        'usuario_idusuario',
        'codigo'
    ];
}
