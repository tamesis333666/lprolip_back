<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeriodoInstitucion extends Model
{
    protected $table = "periodoescolar_has_institucion";
	public $timestamps = false;
}
