<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeriodoUsuario extends Model
{
    protected $table = "usuario_has_periodoescolar";
	public $timestamps = false;
}
