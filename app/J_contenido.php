<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class J_contenido extends Model
{
    protected $table = "j_contenido_juegos";
    protected $primaryKey = 'id_contenido_juego';
}
