<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalleAsignaturas extends Model
{
    protected $table = "salle_asignaturas";
    protected $primaryKey = 'id_asignatura';
}
