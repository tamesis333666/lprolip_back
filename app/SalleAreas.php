<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalleAreas extends Model
{
    protected $table = "salle_areas";
    protected $primaryKey = 'id_area';
}
