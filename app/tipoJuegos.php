<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipoJuegos extends Model
{
    protected $table = "j_tipos_juegos";
    protected $primaryKey = 'id_tipo_juego';
}
