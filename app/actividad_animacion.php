<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class actividad_animacion extends Model
{
    protected $table = "actividades_animaciones";
    protected $primaryKey = 'id_item';
}
